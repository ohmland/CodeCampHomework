class Employee {
    constructor(firstname, lastname, salary) {
        this._salary = salary; // simulate private variable
        this.firstname = firstname; // public property
        this.lastname = lastname; // public property
    }
    getSalary (salary) { // simulate public method
        return this._salary;
    }
    hello() { // simulate public method
        console.log("Hello " + this.firstname + "!");
    }
}

class CEO extends Employee {
    constructor(firstname, lastname, salary) {
        super(firstname, lastname, salary);
    }
    getSalary() { // simulate public method
        return super.getSalary() * 2;
    };
    hello() { // simulate public method
        console.log("Hi, nice to meet you. " + this.firstname + "!");
    }
}

let dang = new Employee('Dang', 'Red', 10000);
let ceo = new CEO('Somchai','Sudlor', 30000);

console.log(ceo.getSalary());