let fs = require('fs');

let temp = fs.readFileSync('homework1.json', 'utf8');

let employees = JSON.parse(temp);

function addYearSalary(row) {
    row['yearSalary'] = parseInt(row.salary) * 12;
}

function addNextSalary(row) {
    let arr = [];
    let baseSalary = parseInt(row.salary);

    for (let i = 0; i < 3; i++) {
        arr[i] = baseSalary;
        baseSalary = baseSalary + (baseSalary * 0.1);
    }
    row['nextSalary'] = arr;
}

function addAdditionalFields(employees) {
    for (let i = 0; i < employees.length; i++) {
        addYearSalary(employees[i]);
        addNextSalary(employees[i]);
    }
}

// addYearSalary(employees[3]);
// addNextSalary(employees[3]);
addAdditionalFields(employees)
console.log(employees);