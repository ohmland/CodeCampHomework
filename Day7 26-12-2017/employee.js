class Employee {
    constructor(firstname, lastname, salary) {
        this._salary = salary; // simulate private variable
        this.firstname = firstname; // public property
        this.lastname = lastname; // public property
    }
    getSalary (salary){ // simulate public method
        return this._salary;
    }
    hello() { // simulate public method
        console.log("Hello " + this.firstname + "!");
    }
}
exports.Employee = Employee;