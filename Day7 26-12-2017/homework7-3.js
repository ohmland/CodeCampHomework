let Input1 = [1, 2, 3]
let Input2 = {
    a: 1,
    b: 2
}
let Input3 = [1, 2, {
    a: 1,
    b: 2
}]
let Input4 = [1, 2, {
    a: 1,
    b: {
        c: 3,
        d: 4
    }
}]

let Input5 = [1, 2, {
    a: 1,
    b: {
        c: 3,
        d: {
            'x': 9,
            'y': 8,
            'z': {
                'u': 'k'
            }
        }
    }
}]

function cloneObj(data) {
    if (data.constructor === Object) {
        let obj = { ...data};
        return obj;
    } else if (data.constructor === Array) {
        let arr = [];
        for (let i = 0; i < data.length; i++) {
            if (typeof data[i] !== 'object') {
                arr[i] = data[i];
            } else {
                let tempObj = {};
                for (let key in data[i]) {
                    if (typeof data[i][key] !== 'object') {
                        tempObj[key] = data[i][key];
                        arr[i] = tempObj;
                    } else {
                        let thirdObj = {};
                        for (let j in data[i][key]) {
                            thirdObj[j] = data[i][key][j];
                            tempObj[key] = thirdObj;
                        }
                        arr[i] = tempObj;
                    }
                }
            }
        }
        return arr;
    }
}

function cloneByPoom(data) {
    if (Array.isArray(data)) {
        return data.map(cloneByPoom);
    } else if (data.constructor === Object) {
        return Object.entries(data).map(([key, value]) => ({
            [key]: cloneByPoom(value)
        })).reduce((prev, cur) => ({ ...prev,
            ...cur
        }))
    }
    return data;
}

//--------------------------------------
let newObj = cloneObj(Input1);
newObj[0] = 5;
console.log(newObj);
console.log('-----------------------');
console.log(Input1);
console.log();




// let newObj= cloneObj([1,2,{a:1,b:2}]);