module.exports = {   
    list,
    create,
    find,
    changeContent,
    remove,
    markComplete,
    markIncomplete
  };
  
  async function list (db) {
    return [result] = await db.execute('select * from todos');
  }

  async function create (db, name) {
    return [result] = await db.execute(`insert into todos (name, txt_status) values (?, ?)`, [name, `Incomplete`]);
  }
  
  async function find (db, id) {
    return [result] = await db.execute('select * from todos where id = ?', [id]);
  }
  
  async function changeContent (db, id, name) {
    return await db.execute(`update todos set name = ? where id = ?`, [name, id]);
  }
  
  async function remove (db, id) {
    return await db.execute(`delete from todos where id = ?`, [id]);
  }
  
  async function markComplete (db, id) {
    return await db.execute(`update todos set txt_status = ? where id = ?`, [`Complete`, id]);
  }
  
  async function markIncomplete (db, id) {
    return await db.execute(`update todos set txt_status = ? where id = ?`, [`Incomplete`, id]);
  }