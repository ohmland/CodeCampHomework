create table todos (
    id int auto_increment,
    name varchar(255),
    txt_status varchar(50),
    create_at timestamp default now(),
    primary key (id)
);