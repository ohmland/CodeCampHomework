module.exports = function (pool, repo) {
    return {
        async list(ctx) {
            //ctx.body = await repo.list(pool);
            let data = await repo.list(pool);
            //data = data[0];
            ctx.body = data[0];
        },
        async create(ctx) {
            // TODO: validate todo
            let {name} = ctx.request.body;
            if(name){
                await repo.create(pool, name);
                ctx.redirect('/todo');
            }
        },
        async get(ctx) {
            const id = ctx.params.id;
            // TODO: validate id
            if(id){
                // find todo from repo
                const [result] = await repo.find(pool, id);
                ctx.body = [result];
                // send todo
            }
        },
        async update(ctx) {
            const id = ctx.params.id;
            let {name} = ctx.request.body;
            // TODO: validate id
            if(id && name){
                // find todo from repo
                await repo.changeContent(pool, id, name);
                // send todo
                ctx.redirect('/todo');
                // let data = await repo.list(pool);
                // data = data[0];
                // ctx.body = data[0];
            }
        },
        async delete(ctx) {
            const id = ctx.params.id;
            // TODO: validate id
            if(id){
                // find todo from repo
                await repo.remove(pool, id);
                //ctx.body = [result];
                // send todo
                ctx.redirect('/todo');
            }
        },
        async complete(ctx) {
            const id = ctx.params.id;
            // TODO: validate id
            if(id){
                // find todo from repo
                await repo.markComplete(pool, id);
                //ctx.body = [result];
                // send todo
                ctx.redirect('/todo');
            }
        },
        async incomplete(ctx) {
            const id = ctx.params.id;
            // TODO: validate id
            if(id){
                // find todo from repo
                await repo.markIncomplete(pool, id);
                //ctx.body = [result];
                // send todo
                ctx.redirect('/todo');
            }
        },
    }
}