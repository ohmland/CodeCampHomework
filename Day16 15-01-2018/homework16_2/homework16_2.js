const Koa = require('koa');
const Router = require('koa-router');
const router = new Router()
const app = new Koa();
//Twitter Routes

//Auth
router.post('/auth/signup')
router.post('/auth/signin')
router.get('/auth/signout')
router.post('/auth/verify')

//Upload
router.post('/auth/upload')

//User
router.patch('/user/:id')
router.put('/user/:id/follow')
router.delete('/user/:id/follow')
router.get('/user/:id/follow')
router.get('/user/:id/followed')

//Tweet
router.get('/tweet')
router.post('/tweet')
router.put('/tweet/:id/like')
router.delete('/tweet/:id/like')
router.post('/tweet/:id/retweet')
router.put('/tweet/:id/vote/:voteId')
router.post('/tweet/:id/reply')

//Notification
router.get('/notification')

//Direct Message
router.get('/message')
router.get('/message/:userId')
router.post('/message/:userId')

app.use(requestLogger);
app.use(router.routes()).listen(4000);

async function requestLogger (ctx, next) {
    console.log(`${ctx.method} ${ctx.path}`)
    await next()
}