let fs = require('fs');

function readHead() {
    return new Promise(function(resolve, reject) {
    fs.readFile('head.txt', 'utf8', function(err, head) {
        if (err)
            reject(err);
        else
            resolve(head);
        });
    });
}

function readBody() {
    return new Promise(function(resolve, reject) {
    fs.readFile('Body.txt', 'utf8', function(err, body) {
        if (err)
            reject(err);
        else
            resolve(body);
        });
    });
}

function readLeg() {
    return new Promise(function(resolve, reject) {
    fs.readFile('leg.txt', 'utf8', function(err, leg) {
        if (err)
            reject(err);
        else
            resolve(leg);
        });
    });
}

function readFeet() {
    return new Promise(function(resolve, reject) {
    fs.readFile('feet.txt', 'utf8', function(err, feet) {
        if (err)
            reject(err);
        else
            resolve(feet);
        });
    });
}

function writeRobot(dataHead, dataBody, dataLeg, dataFeet) {
    return new Promise(function(resolve, reject) {
    fs.writeFile('robot3_2.txt', dataHead+'\n'+dataBody+'\n'+dataLeg+'\n'+dataFeet, 'utf8', function(err) {
        if (err)
            reject(err);
        else
            resolve();
        });
    });
}

    async function copyFile() {
        try {
            let dataHead = await readHead();
            let dataBody = await readBody();
            let dataLeg = await readLeg();
            let dataFeet = await readFeet();
            await writeRobot(dataHead, dataBody, dataLeg, dataFeet);
        } catch (error) {
            console.error(error);
        }
    }
    copyFile();