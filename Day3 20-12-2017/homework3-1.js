    let fs = require('fs');

    let t1 = new Promise(function(resolve, reject){
        fs.readFile('head.txt', 'utf8', function(err, head) {
            if (err)
                reject(err);
            else
            resolve(head);
            });
    });

    let t2 = new Promise(function(resolve, reject){
        fs.readFile('body.txt', 'utf8', function(err, body) {
            if (err)
                reject(err);
            else
            resolve(body);
            });
    });

    let t3 = new Promise(function(resolve, reject){
        fs.readFile('leg.txt', 'utf8', function(err, leg) {
            if (err)
                reject(err);
            else
            resolve(leg);
            });
    });

    let t4 = new Promise(function(resolve, reject){
        fs.readFile('feet.txt', 'utf8', function(err, feet) {
            if (err)
                reject(err);
            else
            resolve(feet);
            });
    });

    Promise.all([t1, t2, t3, t4])
    .then(function(result){
        fs.writeFile('robot.txt',result[0]+'\n'+result[1]+'\n'+result[2]+'\n'+result[3],'utf8', function(err){
            console.log(result[0]+'\n'+result[1]+'\n'+result[2]+'\n'+result[3]);                    
        });
    }).catch(function(error){
        console.error("There's an error", error);
        });