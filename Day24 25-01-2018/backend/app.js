const Koa = require('koa');
const app = new Koa();
const mysql = require('mysql2/promise');

const bodyParser = require('koa-bodyparser');
const Router = require('koa-router');
const cors = require('@koa/cors');

let main = async ()=>{
let db = await mysql.createConnection({
    "localhost" : "127.0.0.1",
    "user" : "root",
    "database" : "bookshopdb"
    }
);

await db.execute('select * from booktb');

let router = new Router();
router.get('/list/Book', async (ctx)=>{
    let [result] = await db.execute(`select * from booktb;`);
    ctx.body = result;
})
.post('/add/Book', async (ctx)=>{
    let {isbn, bookName, price, promoDate, imgPath} = ctx.request.body;
    await db.execute(`INSERT INTO booktb (isbn, bookname, price, promotion_date, image) VALUES (?, ?, ?, ?, ?);`,[isbn, bookName, price, promoDate, imgPath]);
})
.post('/edit/Book', async (ctx)=>{
    let {isbn, bookName, price, promoDate, imgPath} = ctx.request.body;
    await db.execute(`UPDATE booktb SET bookname = ?, price = ?, promotion_date = ?, image = ? WHERE isbn = ?;`,[bookName, price, promoDate, imgPath, isbn]);
})
.get('/delete/Book/:id', async (ctx)=>{
    let isbn = ctx.params.id;
    //let {isbn, bookName, price, promoDate, imgPath} = ctx.request.body;
    await db.execute(`DELETE FROM booktb WHERE isbn = ?;`,[isbn]);
})


app.use(cors());
app.use(bodyParser());
app.use(router.routes());

app.listen(3000);
}

main();