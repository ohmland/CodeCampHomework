let globalVarId;


document.getElementById("btnSave").addEventListener("click", function(){
    let bookNewId = document.getElementById("bookId").value;
    let bookNewTitle = document.getElementById("title").value;
    let bookNewSalePrice = document.getElementById("salePrice").value;
    let bookNewPromoDate = document.getElementById("promoDate").value;
    let bookNewImgPath = document.getElementById("imgPath").value;
    
    let status =document.getElementById("modalHeader").innerText;
    
    if(status == "ADD BOOK"){
        let rowNewItem = document.getElementById("rowItem");
        rowNewItem.innerHTML += `<div id="${bookNewId}" class="col-lg-3 col-md-4 col-sm-6">
                                    <div class="item">
                                            <input id="bookID_${bookNewId}" type="hidden" name="country" value="${bookNewId}">
                                            <input id="salePrice_${bookNewId}" type="hidden" name="country" value="${bookNewSalePrice}">
                                            <input id="promoDate_${bookNewId}" type="hidden" name="country" value="${bookNewPromoDate}">
                                        <div>
                                            <H1 id="title_${bookNewId}">${bookNewTitle}</H1>
                                        </div>
                                        <div>
                                            <img id="img_${bookNewId}" src="${bookNewImgPath}" width="180px" height="250px">
                                        </div>
                                        <div class="form-inline margintop">
                                                Price : <span id="price_${bookNewId}" class="card">${bookNewSalePrice}</span>$
                                            <div class="input-group">
                                                <button id="btnEditItem" type="button" name="btnEdit" class="btn btn-success" data-toggle="modal" data-target="#myModal"
                                                    data-id="${bookNewId}" onclick="apiEditBook('${bookNewId}')">Edit Item
                                                    <i class="glyphicon glyphicon-edit"></i>
                                                </button>
                                            </div>
                                            <div class="input-group">
                                                <button id="btnDeleteItem" type="button" class="btn btn-warning" title="add button" onclick="apiDeleteBook('${bookNewId}')">
                                                    <i class="glyphicon glyphicon-minus"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>`
    }
    else if(status == "EDIT BOOK"){
        let bookEdit_id = "bookID_"+globalVarId;
        let titleEdit_id = "title_"+globalVarId;
        let salePriceEdit_id = "price_"+globalVarId;
        let promoDateEdit_id = "promoDate_"+globalVarId;
        let imgPathEdit_id = "img_"+globalVarId;
        
        document.getElementById(bookEdit_id).value = document.getElementById("bookId").value;
        document.getElementById(titleEdit_id).innerText = document.getElementById("title").value;
        document.getElementById(salePriceEdit_id).innerText = document.getElementById("salePrice").value;
        document.getElementById(promoDateEdit_id).value = document.getElementById("promoDate").value;
        document.getElementById(imgPathEdit_id).src = document.getElementById("imgPath").value;
    }

    //Clear value in modal
    document.getElementById("bookId").value = "";
    document.getElementById("title").value = "";
    document.getElementById("salePrice").value = "";
    document.getElementById("promoDate").value = "";
    document.getElementById("imgPath").value = "";
});



//Edit Button In Modal
function editFunction(id) {
    //id.getAttribute("data-id")
    globalVarId = id;
    document.getElementById("modalHeader").innerText="EDIT BOOK";

    let bookEdit_id = "bookID_"+id;
    let titleEdit_id = "title_"+id;
    let salePriceEdit_id = "price_"+id;
    let promoDateEdit_id = "promoDate_"+id;
    let imgPathEdit_id = "img_"+id;
    
    document.getElementById("bookId").value = document.getElementById(bookEdit_id).value;
    document.getElementById("title").value = document.getElementById(titleEdit_id).innerText;
    document.getElementById("salePrice").value = document.getElementById(salePriceEdit_id).innerText;
    document.getElementById("promoDate").value = document.getElementById(promoDateEdit_id).value;
    document.getElementById("imgPath").value = document.getElementById(imgPathEdit_id).src; 
}

function deleteFunction(id) {
    var r = confirm('Are you sure to delete item '+id+' y/n ')
    if(r == true){
        let elem = document.getElementById(id);
        let parent = document.getElementById("rowItem");
        parent.removeChild(elem);
    }else{

    }
}

window.editFunction = editFunction
window.deleteFunction = deleteFunction


document.getElementById("btnAddItem").addEventListener("click", function(){
    document.getElementById("modalHeader").innerText="ADD BOOK";

    document.getElementById("bookId").value = "";
    document.getElementById("title").value = "";
    document.getElementById("salePrice").value = "";
    document.getElementById("promoDate").value = "";
    document.getElementById("imgPath").value = "";
});
/*let btnEdit = document.getElementsByName("btnEdit")
btnEdit.forEach(b => b.addEventListener('click', () => myFunction()))
console.log(btnEdit)*/


window.apiAddBook = function() {
    
    let status = document.getElementById("modalHeader").innerText;
    if(status == "ADD BOOK") {
        let url = 'http://localhost:3000/add/Book';
        let data = {isbn: document.getElementById("bookId").value,
                    bookName : document.getElementById("title").value,
                    price : document.getElementById("salePrice").value,
                    promoDate : document.getElementById("promoDate").value,
                    imgPath : document.getElementById("imgPath").value
        };
        
        fetch(url, {
        method: 'post', // or 'PUT'
        body: JSON.stringify(data),
        headers: new Headers({
        'Content-Type': 'application/json'
        })
        }).then(res => res.json())
        .catch(error => console.error('Error:', error))
        .then(response => {
            let rowNewItem = document.getElementById("rowItem");
            rowNewItem.innerHTML = "";
            apiListBook();
            })
    }
    else if(status == "EDIT BOOK"){
        let url = 'http://localhost:3000/edit/Book';
        let data = {isbn: document.getElementById("bookId").value,
                    bookName : document.getElementById("title").value,
                    price : document.getElementById("salePrice").value,
                    promoDate : document.getElementById("promoDate").value,
                    imgPath : document.getElementById("imgPath").value
        };
        console.log(data);
        
        fetch(url, {
        method: 'post', // or 'PUT'
        body: JSON.stringify(data),
        headers: new Headers({
        'Content-Type': 'application/json'
        })
        }).then(res => res.json())
        .catch(error => console.error('Error:', error))
        .then(response => console.log('Success:', response))
    }

}

window.apiDeleteBook = function(id) {
    var r = confirm('Are you sure to delete item '+id+' y/n ')
    if(r == true){
        let url = 'http://localhost:3000/delete/Book/' + id;
        fetch(url, {
        method: 'get', // or 'PUT'
        headers: new Headers({
        'Content-Type': 'application/json'
        })
        }).then(res => res.json())
        .catch(error => console.error('Error:', error))
        .then(response => {

            let rowNewItem = document.getElementById("rowItem");
            rowNewItem.innerHTML = "";
            apiListBook();

            console.log('Success:', response)}
        )
    }else{

    }

}

window.apiListBook = function() {
    let url = 'http://localhost:3000/list/Book';
    
    fetch(url, {
     method: 'get', // or 'PUT'
     headers: new Headers({
       'Content-Type': 'application/json'
     })
    }).then(res => res.json())
    .then(data =>{
        data.forEach(element => {
            
            let rowNewItem = document.getElementById("rowItem");
            rowNewItem.innerHTML += `<div id="${element.isbn}" class="col-lg-3 col-md-4 col-sm-6">
                                        <div class="item">
                                                <input id="bookID_${element.isbn}" type="hidden" name="country" value="${element.isbn}">
                                                <input id="salePrice_${element.isbn}" type="hidden" name="country" value="${element.price}">
                                                <input id="promoDate_${element.isbn}" type="hidden" name="country" value="${element.promotion_date}">
                                            <div>
                                                <H1 id="title_${element.isbn}">${element.bookname}</H1>
                                            </div>
                                            <div>
                                                <img id="img_${element.isbn}" src="${element.image}" width="180px" height="250px">
                                            </div>
                                            <div class="form-inline margintop">
                                                    Price : <span id="price_${element.isbn}" class="card">${element.price}</span>$
                                                <div class="input-group">
                                                    <button id="btnEditItem" type="button" name="btnEdit" class="btn btn-success" data-toggle="modal" data-target="#myModal"
                                                        data-id="${element.isbn}" onclick="editFunction('${element.isbn}')">Edit Item
                                                        <i class="glyphicon glyphicon-edit"></i>
                                                    </button>
                                                </div>
                                                <div class="input-group">
                                                    <button id="btnDeleteItem" type="button" class="btn btn-warning" title="add button" onclick="apiDeleteBook('${element.isbn}')">
                                                        <i class="glyphicon glyphicon-minus"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>`
        });
        
    })
    .catch(error => console.error('Error:', error))
    .then(response => console.log('Success:', response))
}

apiListBook();