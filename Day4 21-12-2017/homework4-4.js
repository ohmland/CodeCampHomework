let fs = require('fs');

fs.readFile('homework1-4.json','utf8', (err,data) => {
    let temp1 = JSON.parse(data);
    console.log(
    
    temp1
    .filter(filterGender)
    .map(cutColumn)
    .map(decreaseSalary)
    );   
});


function filterGender(arr){
    return arr.gender === 'male' && arr.friends.length > 1;
}
function cutColumn(arr){
    const {name,gender,company,email,friends,balance} = arr;
    return {name,gender,company,email,friends,balance};
}
function decreaseSalary(arr){
    arr.balance = '$'+arr.balance.replace(',','').replace('$','')/10;
    return arr;
}

