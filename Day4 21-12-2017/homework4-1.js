// const result = arr.filter(arrTemp => {
//         return arrTemp % 2 == 0;
//     });
// console.log(result);

let arr= [1,2,3,4,5,6,7,8,9,10];
function mod(num) {
    return (num % 2 == 0);
}
function multiply(num) {
    return num * 1e3;
}

const result = arr
.filter(mod)
.map(multiply)
console.log(result);

//2000 4000 6000 8000 10000