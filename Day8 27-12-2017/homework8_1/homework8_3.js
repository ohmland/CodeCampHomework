const {
    Employee
} = require('./employee.js');
const {
    Programmer
} = require('./Programmer.js');
const {
    CEO
} = require('./Ceo.js');

//let employees[] = new CEO
let nakorn = new CEO("Nakorn", "Bun", 60000);

nakorn.readFile(function fcName(err, data) {
    if (err) {
        console.error(err);
    } else {
        let arrEmployee = [];
        for (let i in data) {
            arrEmployee[i] = new Programmer(data[i].firstname, data[i].lastname, data[i].salary, data[i].id);
        }
        console.log(arrEmployee);
    }
});