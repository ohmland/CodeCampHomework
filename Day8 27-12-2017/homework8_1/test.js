// class Database {
//     constructor(host, username, password) {
//         this.host = host;
//         this.username = username;
//         this.password = password;
//     }
//     static getInstance() {
//         if (!this._instance)
//             this._instance = new Database('localhost', 'root','12345');
//                 return this._instance;
//     }
// }
// let db = Database.getInstance();

// const mysql = require('mysql');
// class Database {
//     constructor(){
//         this.connection = mysql.createConnection({
//             host    : 'localhost',
//             user    : 'me',
//             password: 'secret',
//             database: 'my_db'
//         });
//     }
//     getUsers(callbackFunction){
//         let sql = "SELECT * FROM user";
//         this.connection.query(sql, function (err, result)){
//             if(err)
//                 console.error(err);
//             else
//                 console.log(result);
//         }
//     }
// }
// const db = new Database();
// db.getUsers(callbackFunction);
let fs = require('fs');

function copyFile(filename, callback) {
    fs.readFile(filename, 'utf8', function (err, data) {
        fs.writeFile(filename, data, 'utf8', function (err) {
            if (err)
                callback(err);
            else
                callback(null);
        });
    });
}