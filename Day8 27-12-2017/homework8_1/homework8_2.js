class MobilePhone {
    PhoneCall(){

    }
    SMS(){
        
    }
    InternetSurfing(){

    }   
}

class Samsung extends MobilePhone {
    UseGearVR(){

    }
    TransformToPC(){

    }
    GooglePlay(){

    }
}

class Apple extends MobilePhone {
    AppStore(){

    }
}

class GalaxyNote8 extends Samsung {
    UsePen(){

    }
}

let galaxyS8 = new Samsung();

// class GalaxyS8 extends Samsung {
    
// }

class iPhoneX extends Apple {
    FaceTime(){

    }
}

class iPhone8 extends Apple {
    TouchID(){

    }
}