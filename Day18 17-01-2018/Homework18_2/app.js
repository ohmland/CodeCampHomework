const Koa = require('koa');
const app = new Koa();
const session = require('koa-session');
const path = require('path');
const config = require('./config/database.json');
const {db} = require("./lib/db.js");
const mysess = require("./models/mysess.js")(db);
const bodyParser = require('koa-bodyparser');
const bcrypt = require('bcryptjs');
const Router = require('koa-router');
//const sessionStore = {};
//const mysess = require("./models/mysess.js")(db);

const sessionConfig = {
    key: 'sess',
    maxAge: 3600 * 1000,
    httpOnly: true,
    store: mysess
};

const router = new Router()
    //Authen Route
    .get('/', index)
    .post('/auth/signin', login)
    .get('/auth/signout', logout)
    .get('/auth/check', checkSession)
    
    //Direct Message Route
    .get('/message', getMessageAll)
    .get('/message/:userId', readMessage)
    .post('/message/:userId', sendMessage)

    //notification
    .get('/notification', getNotification)
    
    //Follow
    .put('/user/:id/follow', follow)
    .delete('/user/:id/follow', unfollow)
    .get('/user/:id/follow', followList)
    .get('/user/:id/followed', followedList)


//Authen Function
async function index(ctx) {
    ctx.body = "index;"
}

async function login(ctx) {
    const {email, password} = ctx.request.body;
    const [result] = await db.connection.execute("select * from `users` where email = ? and password = ?", [email, password]);
    let data = [result][0];
        
    if(data){
        let data = [result][0];
        ctx.session.email = data[0].email;
        //ctx.session.id = data[0].id;
        ctx.status = 200;
        ctx.body = {};
    }else{
        ctx.status = 401;
        ctx.body = {"error": "wrong password"}
    }
}

async function logout(ctx) {
    ctx.status = 200;
    ctx.session = null;
    ctx.body = {};
}

async function checkSession(ctx) {
    if(ctx.session.email){
        ctx.status = 200;
        ctx.body = {};
    }else{
        ctx.status = 401;
        ctx.body = {
            "error": "unauthorized"
        };
    }
}

//Direct Message Function
async function getMessageAll(ctx) {
    //List all direct messages
    if(ctx.session.email){
        ctx.status = 200;
        const [userResult] = await db.connection.execute("select * from users where email = ?", [ctx.session.email]);
        let dataUser = [userResult][0];
        const [result] = await db.connection.execute("select uc.id, uc.sender_id, uc.receiver_id, uc.content, uc.type, uc.created_at from user_chats uc left join users u on uc.sender_id = u.id where uc.sender_id = ? or uc.receiver_id = ?", [dataUser[0].id, dataUser[0].id]);
        let dataMessage = [result][0];
        ctx.body = dataMessage;
    }else{
        ctx.status = 401;
        ctx.body = {
            "error": "unauthorized"
        };
    }
}

async function readMessage(ctx) {
    //แสดงข้อความทั้งหมดสำหรับห้องผู้ใช้ที่ระบุ
    if(ctx.session.email){
        let userId = ctx.params.userId;
        if(userId) {
            ctx.status = 200;
            const [result] = await db.connection.execute("select uc.id, uc.sender_id, uc.receiver_id, uc.content, uc.type, uc.created_at from user_chats uc left join users u on uc.sender_id = u.id where uc.sender_id = ? or uc.receiver_id = ?", [userId, userId]);
            let dataMessage = [result][0];
            ctx.body = dataMessage;
            return
        }
        
        ctx.body = {};
    }else{
        ctx.status = 401;
        ctx.body = {
            "error": "unauthorized"
        };
    }
}

async function sendMessage(ctx) {
    //ส่งข้อความตรงไปยังผู้ใช้ที่ระบุ
    if(ctx.session.email){
        let receiverId = ctx.params.userId;
        if(receiverId) {
            let { content, type } = ctx.request.body;
            if((content) && (type)) {
                const [userResult] = await db.connection.execute("select * from users where email = ?", [ctx.session.email]);
                let dataUser = [userResult][0];

                await db.connection.execute("insert into user_chats (sender_id, receiver_id, content, type) values (?, ?, ?, ?)", [dataUser[0].id, receiverId, content, type]);
                //let dataMessage = [result][0];

                let noti = {"title": "Chat","content": dataUser[0].username, "photo":""};
                await db.connection.execute("insert into notifications (user_id, title, content, photo) values (?, ?, ?, ?)", [receiverId, noti.title, noti.content, noti.photo]);

                ctx.status = 200;
                ctx.body = {};
            }
        }    
    }
    else{
        ctx.status = 401;
        ctx.body = {
            "error": "unauthorized"
        };
    }
}

async function getNotification(ctx) {
    if(ctx.session.email){
        //const [result] = await db.connection.execute("select * from notifications");
        const [result] = await db.connection.execute("select  u.name, n.title, n.created_at from notifications n left join users u on n.user_id = u.id");
        let data = [result][0];
            
        if(data){
            let data = [result][0];
            //let obj = {};
            for (let i = 0; i < data.length; i++) {
                //obj = obj + ' ' + data[i].name + ' ' + data[i].title + ' on time' + data[i].created_at;
                console.log(data[i].name + ' ' + data[i].title + ' on time ' + data[i].created_at);    
            }
            ctx.body = {};
            
        }
    }
    else{
        ctx.status = 401;
        ctx.body = {"error": "unauthorized"};
    }
}

async function follow(ctx) {
    if(ctx.session.email){
        let id = ctx.params.id;
        if(id) {
            const [userResult] = await db.connection.execute("select * from users where email = ?", [ctx.session.email]);
            let dataUser = [userResult][0];

            await db.connection.execute("insert into follows (follower_id, following_id) values (?, ?)", [dataUser[0].id, id]);

            let noti = {"title": "follow","content": "follow", "photo":""};

            await db.connection.execute("insert into notifications (user_id, title, content, photo) values (?, ?, ?, ?)", [id, noti.title, noti.content, noti.photo]);
            ctx.status = 200;
            ctx.body = {};
        }
    }
    else{
        ctx.status = 401;
        ctx.body = {"error": "unauthorized"};
    }
}

async function unfollow(ctx) {
    if(ctx.session.email){
        let id = ctx.params.id;
        if(id) {
            const [userResult] = await db.connection.execute("select * from users where email = ?", [ctx.session.email]);
            let dataUser = [userResult][0];

            await db.connection.execute("delete from follows where follower_id = ? and following_id = ?", [dataUser[0].id, id]);
   
            ctx.status = 200;
            ctx.body = {};
        }
    }
    else{
        ctx.status = 401;
        ctx.body = {"error": "unauthorized"};
    }
}

async function followList(ctx) {
    if(ctx.session.email){
        let id = ctx.params.id;
        if(id) {
            let [result] = await db.connection.execute("select id, username, photo, cover, bio from follows fw left join users us on us.id = fw.follower_id where fw.following_id = ?", [id]);
            let dataUser = [result][0];

            ctx.status = 200;
            ctx.body = {dataUser};
        }
    }
    else{
        ctx.status = 401;
        ctx.body = {"error": "unauthorized"};
    }
}

async function followedList(ctx) {
    if(ctx.session.email){
        let id = ctx.params.id;
        if(id) {
            let [result] = await db.connection.execute("select id, username, photo, cover, bio from follows fw left join users us on us.id = fw.following_id where fw.follower_id = ?", [id])
            let dataUser = [result][0];

            ctx.status = 200;
            ctx.body = {dataUser};
        }
    }
    else{
        ctx.status = 401;
        ctx.body = {"error": "unauthorized"};
    }
}

app.keys = ['supersecret'];
app.use(bodyParser())
    .use(session(sessionConfig, app))
    .use(router.routes()).listen(3000);