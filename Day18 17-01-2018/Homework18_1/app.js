const Koa = require('koa');
const Router = require('koa-router');
const multer = require('koa-multer');
const Jimp = require("jimp");
const fs = require('fs');
const bodyParser = require('koa-bodyparser')
const app = new Koa();

const upload = multer({dest: 'upload/'});
app.use(bodyParser());
const router = new Router()

router.post("/upload", uploadFile);

app.use(router.routes()).listen(4000);


async function uploadFile(ctx) {
    await upload.single('file')(ctx)
    //ctx.body = ctx.req.file.path
    const tempFile = ctx.req.file.path
    const outFile = tempFile.substring(0, 7) + 'pic_' + new Date().getMonth() + new Date().getDate() + new Date().getHours() + new Date().getMinutes() + new Date().getSeconds() + '.jpg'
    Jimp.read(tempFile, function (err, tempFile) {
        if (err) throw err;
        tempFile.resize(256, 256)            // resize 
             .quality(60)                 // set JPEG quality 
             //.greyscale()                 // set greyscale 
             .write(outFile); // save 
             ctx.body = {};
    });

}