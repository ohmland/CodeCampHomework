let globalVarId;
document.getElementById("btnSave").addEventListener("click", function(){
    let bookNewId = document.getElementById("bookId").value;
    let bookNewTitle = document.getElementById("title").value;
    let bookNewSalePrice = document.getElementById("salePrice").value;
    let bookNewPromoDate = document.getElementById("promoDate").value;
    let bookNewImgPath = document.getElementById("imgPath").value;
    
    let status =document.getElementById("modalHeader").innerText;
    
    if(status == "ADD BOOK"){
        let rowNewItem = document.getElementById("rowItem");
        rowNewItem.innerHTML += `<div id="${bookNewId}" class="col-lg-3 col-md-4 col-sm-6">
                                    <div class="item">
                                            <input id="bookID_${bookNewId}" type="hidden" name="country" value="${bookNewId}">
                                            <input id="salePrice_${bookNewId}" type="hidden" name="country" value="${bookNewSalePrice}">
                                            <input id="promoDate_${bookNewId}" type="hidden" name="country" value="${bookNewPromoDate}">
                                        <div>
                                            <H1 id="title_${bookNewId}">${bookNewTitle}</H1>
                                        </div>
                                        <div>
                                            <img id="img_${bookNewId}" src="${bookNewImgPath}" width="180px" height="250px">
                                        </div>
                                        <div class="form-inline margintop">
                                                Price : <span id="price_${bookNewId}" class="card">${bookNewSalePrice}</span>$
                                            <div class="input-group">
                                                <button id="btnEditItem" type="button" name="btnEdit" class="btn btn-success" data-toggle="modal" data-target="#myModal"
                                                    data-id="${bookNewId}" onclick="editFunction('${bookNewId}')">Edit Item
                                                    <i class="glyphicon glyphicon-edit"></i>
                                                </button>
                                            </div>
                                            <div class="input-group">
                                                <button id="btnDeleteItem" type="button" class="btn btn-warning" title="add button" onclick="deleteFunction('${bookNewId}')">
                                                    <i class="glyphicon glyphicon-minus"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>`
    }
    else if(status == "EDIT BOOK"){
        //document.getElementById(globalVarId).innerHTML = 
        
        let bookEdit_id = "bookID_"+globalVarId;
        let titleEdit_id = "title_"+globalVarId;
        let salePriceEdit_id = "price_"+globalVarId;
        let promoDateEdit_id = "promoDate_"+globalVarId;
        let imgPathEdit_id = "img_"+globalVarId;
        
        document.getElementById(bookEdit_id).value = document.getElementById("bookId").value;
        document.getElementById(titleEdit_id).innerText = document.getElementById("title").value;
        document.getElementById(salePriceEdit_id).innerText = document.getElementById("salePrice").value;
        document.getElementById(promoDateEdit_id).value = document.getElementById("promoDate").value;
        document.getElementById(imgPathEdit_id).src = document.getElementById("imgPath").value;
    }

    //Clear value in modal
    document.getElementById("bookId").value = "";
    document.getElementById("title").value = "";
    document.getElementById("salePrice").value = "";
    document.getElementById("promoDate").value = "";
    document.getElementById("imgPath").value = "";
});


//Edit Button In Modal
function editFunction(id) {
    //id.getAttribute("data-id")
    globalVarId = id;
    document.getElementById("modalHeader").innerText="EDIT BOOK";

    let bookEdit_id = "bookID_"+id;
    let titleEdit_id = "title_"+id;
    let salePriceEdit_id = "price_"+id;
    let promoDateEdit_id = "promoDate_"+id;
    let imgPathEdit_id = "img_"+id;
    
    document.getElementById("bookId").value = document.getElementById(bookEdit_id).value;
    document.getElementById("title").value = document.getElementById(titleEdit_id).innerText;
    document.getElementById("salePrice").value = document.getElementById(salePriceEdit_id).innerText;
    document.getElementById("promoDate").value = document.getElementById(promoDateEdit_id).value;
    document.getElementById("imgPath").value = document.getElementById(imgPathEdit_id).src; 
}

function deleteFunction(id) {
    var r = confirm('Are you sure to delete item '+id+' y/n ')
    if(r == true){
        let elem = document.getElementById(id);
        let parent = document.getElementById("rowItem");
        parent.removeChild(elem);
    }else{

    }
}

window.editFunction = editFunction
window.deleteFunction = deleteFunction


document.getElementById("btnAddItem").addEventListener("click", function(){
    document.getElementById("modalHeader").innerText="ADD BOOK";

    document.getElementById("bookId").value = "";
    document.getElementById("title").value = "";
    document.getElementById("salePrice").value = "";
    document.getElementById("promoDate").value = "";
    document.getElementById("imgPath").value = "";
});
/*let btnEdit = document.getElementsByName("btnEdit")
btnEdit.forEach(b => b.addEventListener('click', () => myFunction()))
console.log(btnEdit)*/