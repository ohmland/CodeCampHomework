/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "dist";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var globalVarId = void 0;
document.getElementById("btnSave").addEventListener("click", function () {
    var bookNewId = document.getElementById("bookId").value;
    var bookNewTitle = document.getElementById("title").value;
    var bookNewSalePrice = document.getElementById("salePrice").value;
    var bookNewPromoDate = document.getElementById("promoDate").value;
    var bookNewImgPath = document.getElementById("imgPath").value;

    var status = document.getElementById("modalHeader").innerText;

    if (status == "ADD BOOK") {
        var rowNewItem = document.getElementById("rowItem");
        rowNewItem.innerHTML += "<div id=\"" + bookNewId + "\" class=\"col-lg-3 col-md-4 col-sm-6\">\n                                    <div class=\"item\">\n                                            <input id=\"bookID_" + bookNewId + "\" type=\"hidden\" name=\"country\" value=\"" + bookNewId + "\">\n                                            <input id=\"salePrice_" + bookNewId + "\" type=\"hidden\" name=\"country\" value=\"" + bookNewSalePrice + "\">\n                                            <input id=\"promoDate_" + bookNewId + "\" type=\"hidden\" name=\"country\" value=\"" + bookNewPromoDate + "\">\n                                        <div>\n                                            <H1 id=\"title_" + bookNewId + "\">" + bookNewTitle + "</H1>\n                                        </div>\n                                        <div>\n                                            <img id=\"img_" + bookNewId + "\" src=\"" + bookNewImgPath + "\" width=\"180px\" height=\"250px\">\n                                        </div>\n                                        <div class=\"form-inline margintop\">\n                                                Price : <span id=\"price_" + bookNewId + "\" class=\"card\">" + bookNewSalePrice + "</span>$\n                                            <div class=\"input-group\">\n                                                <button id=\"btnEditItem\" type=\"button\" name=\"btnEdit\" class=\"btn btn-success\" data-toggle=\"modal\" data-target=\"#myModal\"\n                                                    data-id=\"" + bookNewId + "\" onclick=\"editFunction('" + bookNewId + "')\">Edit Item\n                                                    <i class=\"glyphicon glyphicon-edit\"></i>\n                                                </button>\n                                            </div>\n                                            <div class=\"input-group\">\n                                                <button id=\"btnDeleteItem\" type=\"button\" class=\"btn btn-warning\" title=\"add button\" onclick=\"deleteFunction('" + bookNewId + "')\">\n                                                    <i class=\"glyphicon glyphicon-minus\"></i>\n                                                </button>\n                                            </div>\n                                        </div>\n                                    </div>\n                                </div>";
    } else if (status == "EDIT BOOK") {
        //document.getElementById(globalVarId).innerHTML = 

        var bookEdit_id = "bookID_" + globalVarId;
        var titleEdit_id = "title_" + globalVarId;
        var salePriceEdit_id = "price_" + globalVarId;
        var promoDateEdit_id = "promoDate_" + globalVarId;
        var imgPathEdit_id = "img_" + globalVarId;

        document.getElementById(bookEdit_id).value = document.getElementById("bookId").value;
        document.getElementById(titleEdit_id).innerText = document.getElementById("title").value;
        document.getElementById(salePriceEdit_id).innerText = document.getElementById("salePrice").value;
        document.getElementById(promoDateEdit_id).value = document.getElementById("promoDate").value;
        document.getElementById(imgPathEdit_id).src = document.getElementById("imgPath").value;
    }

    //Clear value in modal
    document.getElementById("bookId").value = "";
    document.getElementById("title").value = "";
    document.getElementById("salePrice").value = "";
    document.getElementById("promoDate").value = "";
    document.getElementById("imgPath").value = "";
});

//Edit Button In Modal
function editFunction(id) {
    //id.getAttribute("data-id")
    globalVarId = id;
    document.getElementById("modalHeader").innerText = "EDIT BOOK";

    var bookEdit_id = "bookID_" + id;
    var titleEdit_id = "title_" + id;
    var salePriceEdit_id = "price_" + id;
    var promoDateEdit_id = "promoDate_" + id;
    var imgPathEdit_id = "img_" + id;

    document.getElementById("bookId").value = document.getElementById(bookEdit_id).value;
    document.getElementById("title").value = document.getElementById(titleEdit_id).innerText;
    document.getElementById("salePrice").value = document.getElementById(salePriceEdit_id).innerText;
    document.getElementById("promoDate").value = document.getElementById(promoDateEdit_id).value;
    document.getElementById("imgPath").value = document.getElementById(imgPathEdit_id).src;
}

function deleteFunction(id) {
    var r = confirm('Are you sure to delete item ' + id + ' y/n ');
    if (r == true) {
        var elem = document.getElementById(id);
        var parent = document.getElementById("rowItem");
        parent.removeChild(elem);
    } else {}
}

window.editFunction = editFunction;
window.deleteFunction = deleteFunction;

document.getElementById("btnAddItem").addEventListener("click", function () {
    document.getElementById("modalHeader").innerText = "ADD BOOK";

    document.getElementById("bookId").value = "";
    document.getElementById("title").value = "";
    document.getElementById("salePrice").value = "";
    document.getElementById("promoDate").value = "";
    document.getElementById("imgPath").value = "";
});
/*let btnEdit = document.getElementsByName("btnEdit")
btnEdit.forEach(b => b.addEventListener('click', () => myFunction()))
console.log(btnEdit)*/

/***/ })
/******/ ]);