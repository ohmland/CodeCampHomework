// const mysql = require('mysql2/promise');

// class Database {
//     constructor(connection) {
//         this.connection = connection;
//     }


//     static async getInstance() {
//         if (!this._instance) {
//             const connection = await mysql.createConnection({
//                 host: 'localhost',
//                 user: 'root',
//                 database: 'codecamp'
//             });
//             this._instance = new Database(connection);
//         }
//         return this._instance;
//     }
//     execute(queryStr) {
//         return this.connection.execute(queryStr);
//     }

// }

// exports.Database = Database;

//---------------------------------------------------------------------------
class Database {
    constructor( ) {    
        this.connectDatabase();
    }

    async connectDatabase() {
        // get the client
        const mysql = require('mysql2/promise');
        // create the connection
        this.connection = await mysql.createConnection({
            host: '127.0.0.1',
            user: 'root',
            database: 'codecamp'
        });

    }

    async execute(sql) {
        // query database
        const [rows, fields] = await this.connection.execute(sql);
        //return [{'firstname' : "testFirst"}];
        return rows;
    }
}

module.exports.db = new Database();