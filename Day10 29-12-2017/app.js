const Koa = require('koa');
const app = new Koa();
const render = require('koa-ejs');
const path = require('path');

// get the client
const mysql = require('mysql2/promise');

async function query(){
    let data = [];

    render(app, {
        root: path.join(__dirname, 'views'),
        layout: 'homework10_1',
        viewExt: 'ejs',
        cache: false,
        debug: true
    });
    
    app.use(async(ctx, next) => { //app.use ทั้งก้อน คือ controller ซึ่งสามารถมี app.use ได้หลายตัว
        try {
            await ctx.render('user', {   //โหลด view เข้า controller   //user คือชื่อของ view    ctx.render จะไปแอบ write ค่า body ให้
                "message": data
                //message คือ parameter ตัวที่2 เป็นการโยนค่าเข้าไปใน view ซึ่งการโยนค่าเข้าไปนั้นต้องเป็น object เท่านั้นในชั้นที่1 ส่วนชั้นที่2 จะเป็น array หรืออะไรก็ได้
            });
            await next();
        } catch (err) {
            ctx.status = 400
            ctx.body = `Uh-oh: ${err.message}`
        }
    });


    // create the connection
    const connection = await mysql.createConnection({
        host: 'localhost',
        user: 'root',
        database: 'codecamp'
    });

    // query database
    //const [rows, fields] = await connection.execute('SELECT * FROM user');
    const [rows, fields] = await connection.execute('SELECT * FROM user');
    for (let i = 0; i < rows.length; i++) {
        data[i] = rows[i].id+','+rows[i].firstname+','+rows[i].lastname+','+rows[i].salary+','+rows[i].role;
    }
    
}

query();
app.listen(3000);