// const Koa = require('koa');
// const app = new Koa();
// const render = require('koa-ejs');
// const path = require('path');
// const mysql = require('mysql2/promise');
// require('./controller/routes.js')(app);

// let fs = require('fs');


// async function query(){
//     let data = [];

//     render(app, {
//         root: path.join(__dirname, 'views'),
//         layout: 'homework10_2',
//         viewExt: 'js',
//         cache: false,
//         debug: true
//     });
//     // create the connection

//     const connection = await mysql.createConnection({
//         host: '127.0.0.1',
//         user: 'root',
//         database: 'codecamp'
//     });
//     let data1 = await require('./models/homework10_3.js')(connection);
//     console.log(data1);
    
//     // for (let i = 0; i < data1.length; i++) {
//     //     data1[i] = rows[i].id+','+rows[i].firstname+','+rows[i].lastname+','+rows[i].salary+','+rows[i].role;
//     // }
    
// }

// query();
// app.listen(3200);

//---------------------------------------------------------
// const Koa = require('koa');
// const app = new Koa();
// const render = require('koa-ejs');
// const path = require('path');
// const {Database} = require('./lib/db.js')

// //console.log(db);


// // get the client
// //const mysql = require('mysql2/promise');

// async function query(){
//     const db = await Database.getInstance();
//     let data = [];

//     render(app, {
//         root: path.join(__dirname, 'views'),
//         layout: 'homework10_2',
//         viewExt: 'js',
//         cache: false,
//         debug: true
//     });
    
//     app.use(async(ctx, next) => {
//         try {
//             await ctx.render('user', {
//                 "message": data[0],
//                 "message1": data[1],
//                 "message2": data[2],
//                 "message3": data[3]
//             });
//             await next();
//         } catch (err) {
//             ctx.status = 400
//             ctx.body = `Uh-oh: ${err.message}`
//         }
//     });

    


//     // query database
//     //const [rows, fields] = await connection.execute('SELECT * FROM user');
//     const [rows, fields] = await db.execute('SELECT * FROM user');
//     console.log(rows);
    
//     for (let i = 0; i < rows.length; i++) {
//         data[i] = rows[i].id+','+rows[i].firstname+','+rows[i].lastname+','+rows[i].salary+','+rows[i].role;
//     }
    
// }

// query();
// app.listen(3000);

//---------------------------------------------------------------
const Koa = require('koa');
const app = new Koa();
const render = require('koa-ejs');
const path = require('path');
//const { db } = require('./lib/db.js');
require('./controller/routes.js')(app);

render(app, {
    root: path.join(__dirname, 'views'),
    layout: 'homework10_2',
    viewExt: 'ejs',
    cache: false,
    debug: true
});
    
app.listen(3200);