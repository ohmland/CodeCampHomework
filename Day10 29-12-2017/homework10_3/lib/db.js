// const mysql = require('mysql2/promise');

// class Database {
//     constructor(connection) {
//         this.connection = connection;
//     }


//     static async getInstance() {
//         if (!this._instance) {
//             const connection = await mysql.createConnection({
//                 host: 'localhost',
//                 user: 'root',
//                 database: 'codecamp'
//             });
//             this._instance = new Database(connection);
//         }
//         return this._instance;
//     }
//     execute(queryStr) {
//         return this.connection.execute(queryStr);
//     }

// }

// exports.Database = Database;

//---------------------------------------------------------------------------
let fs = require('fs');
class Database {
    constructor( ) {    
        //this.connectDatabase();
        fs.readFile('homework10_3/config/database.js','utf8',(err, data)=>{
            if(err){
                console.error(err);
            }
            else{
                let dataR = JSON.parse(data);
                this.host = dataR[0].host;
                this.user = dataR[0].user;
                this.database = dataR[0].database;
                this.connectDatabase(this.host,this.user,this.database);
            }
        })
    }

    async connectDatabase(host,user,database) {
        // get the client
        const mysql = require('mysql2/promise');
        // create the connection
        this.connection = await mysql.createConnection({
            host: this.host,
            user: this.user,
            database: this.database
        });

    }

    // async execute(sql) {
    //     // query database
    //     const [rows, fields] = await this.connection.execute(sql);
    //     //return [{'firstname' : "testFirst"}];
    //     return rows;
    // }
}

module.exports.db = new Database();