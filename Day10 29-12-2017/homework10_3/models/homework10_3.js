const { db } = require('../lib/db.js');

// module.exports = async function(){
//         // query database
//         const [rows, fields] = await this.connection.execute('SELECT * FROM user');
//         return rows;
// }

async function execute(){
        const [rows, fields] = await db.connection.execute('SELECT * FROM user');
        return rows;
}

exports.execute = execute;