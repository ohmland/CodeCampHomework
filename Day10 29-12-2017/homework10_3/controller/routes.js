// const mysql = require('mysql2/promise');

// async function query(){

//     // create the connection

//     const connection = await mysql.createConnection({
//         host: '127.0.0.1',
//         user: 'root',
//         database: 'codecamp'
//     });
// }
// module.exports = function (app) {
//     app.use(async(ctx, next) => {
//         try {
//             let data1 = await require(__dirname+'/homework10_3.js')(connection);
//             await ctx.render('user', {
//                 "message": data1[0],
//                 "message1": data1[1],
//                 "message2": data1[2],
//                 "message3": data1[3]
//             });
//             await next();
//         } catch (err) {
//             ctx.status = 400
//             ctx.body = `Uh-oh: ${err.message}`
//         }
//     });   
// }

// query();

//---------------------------------------------------------------
//const { db } = require('../lib/db.js');
let dbModels = require('../models/homework10_3.js');

module.exports = function (app) {
    app.use(async(ctx, next) => {
        try {
            let data = [];
            //const rows = await require('../models/homework10_3.js')();
            const rows = await dbModels.execute();

            for (let i = 0; i < rows.length; i++) {
                data[i] = rows[i].id+','+rows[i].firstname+','+rows[i].lastname+','+rows[i].salary+','+rows[i].role;
            }

            await ctx.render('user', {   //โหลด view เข้า controller   //user คือชื่อของ view    ctx.render จะไปแอบ write ค่า body ให้
                "message": data
                //message คือ parameter ตัวที่2 เป็นการโยนค่าเข้าไปใน view ซึ่งการโยนค่าเข้าไปนั้นต้องเป็น object เท่านั้นในชั้นที่1 ส่วนชั้นที่2 จะเป็น array หรืออะไรก็ได้
            });
            await next();
        } catch (err) {
            ctx.status = 400
            ctx.body = `Uh-oh: ${err.message}`
        }
    });   
}