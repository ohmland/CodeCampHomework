let fields = ['id','firstname','lastname','company','salary'];
let employees = [
    ['1001','Luke','Skywalker','Walt Disney','40000'],
    ['1002','Tony','Stark','Marvel','1000000'],
    ['1003','Somchai','Jaidee','Love2work','20000'],
    ['1004','Monkey D','Luffee','One Piece','9000000']
];

let arr = [];
for (let i = 0; i < employees.length; i++) {
    let obj = {};
    for (let a in fields) {
        obj[fields[a]] = employees[i][a];
    }
    arr.push(obj);
}
console.log(arr);