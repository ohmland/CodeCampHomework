let fs = require('fs');
class Database {
    constructor( ) {    
        fs.readFile('homework15_1/config/database.js','utf8',(err, data)=>{
            if(err){
                console.error(err);
            }
            else{
                let dataR = JSON.parse(data);
                this.host = dataR[0].host;
                this.user = dataR[0].user;
                this.database = dataR[0].database;
                this.connectDatabase(this.host,this.user,this.database);
            }
        })
    }

    async connectDatabase(host,user,database) {
        // get the client
        const mysql = require('mysql2/promise');
        // create the connection
        this.connection = await mysql.createConnection({
            host: this.host,
            user: this.user,
            database: this.database
        });

    }
}

module.exports.db = new Database();