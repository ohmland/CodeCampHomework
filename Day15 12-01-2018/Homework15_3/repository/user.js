function createEntity(row) {
    return {
        id: row.id,
        firstname: row.name,
        username: row.username
    }
}

module.exports = function (db) {
    return {
        async find(id) {
            const [rows] = await db.connection.execute(`select * from users where id = ?`, [id])
            return createEntity(rows[0])
        }
        , async findAll() {
            const [rows] = await db.connection.execute(`select * from users`, [id])
            return rows.map(createEntity)
        }
        , async store(user) {
            if (!user.id) {
                const result = await db.connection.execute(`insert into users (name, username) values (?, ?, ?)`, [user.name, user.username])
                user.id = result.insertId
                return
            }
            else {
                return db.connection.execute(`update users set name = ?, username = ? where id = ?`, [user.name, user.username, user.id])
            }
        }
        , remove(id) {
            return db.connection.execute(`delete from users where id = ?`, [id])
        }
        ,async findByUsername(username) {
            const [rows] = await db.connection.execute(`select * from users where username = ?` ,[username]);
            return rows.map(createEntity)
        }
    }
};