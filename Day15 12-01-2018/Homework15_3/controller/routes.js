let dbModels = require('../models/query.js');
const {db} = require('../lib/db.js');

module.exports = function (app) {
    app.use(async(ctx, next) => {
        try {
            if(ctx.path != '/favicon.ico') {
                const user = require('../repository/user.js')(db);
                
                //test1
                // const user1 = await user.find(1);
                // user1.firstname = 'repository';
                // await user.store(user1);

                //test2
                // const user1 = await user.findAll();
                // console.log(user1);

                //test3
                // const user1 = await user.findByUsername('ohmland');
                // user1[0].firstname = 'test3';
                // await user.store(user1[0]);

                //test4
                //const user1 = await user.find(4);
                //await user.remove(user1.id);
                
                //test5
                // const user1 =  await user.find(3);
                // user1.id = '';
                // user1.firstname = 'john';
                // user1.lastname = 'cena';
                // user1.username = 'wwe';
                // await user.store(user1);
                // const user2 = await user.findByUsername('wwe');
                // console.log(user2);
                

                //-----------------------------------------
            }
            let data = [];
            const rows = await dbModels.execute();

            for (let i = 0; i < rows.length; i++) {
                data[i] = rows[i].id + ',' + rows[i].firstname + ',' + rows[i].lastname + ',' + rows[i].username;
            }    
            
            //console.log(`${ctx.method} ${ctx.path}`);
            await ctx.render('user', { //โหลด view เข้า controller   //user คือชื่อของ view    ctx.render จะไปแอบ write ค่า body ให้
                "message": data
                //message คือ parameter ตัวที่2 เป็นการโยนค่าเข้าไปใน view ซึ่งการโยนค่าเข้าไปนั้นต้องเป็น object เท่านั้นในชั้นที่1 ส่วนชั้นที่2 จะเป็น array หรืออะไรก็ได้
            });
            await next();
        } catch (err) {
            console.error(err);

            ctx.status = 400
            ctx.body = `Uh-oh: ${err.message}`
        }
    });
}