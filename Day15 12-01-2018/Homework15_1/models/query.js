const {db} = require('../lib/db.js');

async function execute() {
        const [rows, fields] = await db.connection.execute('SELECT * FROM user');
        return rows;
}

exports.execute = execute;