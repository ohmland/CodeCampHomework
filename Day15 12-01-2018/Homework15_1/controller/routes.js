let dbModels = require('../models/query.js');

module.exports = function (app) {
    app.use(async(ctx, next) => {
        try {
            if(ctx.path != '/favicon.ico') {
                const {user} = require('../models/user.js');
                
                //test 1 เปลี่ยน firstname
                // const user1 = await user.find(1);
                // user1.firstname = 'tester';
                // await user1.save();

                //test 2 ค้นหา user ทั้งหมด
                // const rowsUser = await user.findAll();
                // console.log(rowsUser);

                //test 3 
                // const user2 = await user.find(2);
                // user2.id = '';
                // user2.firstname = 'tete';
                // user2.lastname = 'bbbetetb';
                // user2.username = 'cceeecc';
                // await user2.save();
                
                //test 4 ลบ
                // const user2 = await user.find(2);
                // await user2.remove()

                //test 5 ค้นหาจาก username
                const user2 = await user.findByUsername('wwe');
                console.log(user2);
                
                //-----------------------------------------
            }
            let data = [];
            const rows = await dbModels.execute();

            for (let i = 0; i < rows.length; i++) {
                data[i] = rows[i].id + ',' + rows[i].firstname + ',' + rows[i].lastname + ',' + rows[i].username;
            }
            //console.log(`${ctx.method} ${ctx.path}`);
            await ctx.render('user', { //โหลด view เข้า controller   //user คือชื่อของ view    ctx.render จะไปแอบ write ค่า body ให้
                "message": data
                //message คือ parameter ตัวที่2 เป็นการโยนค่าเข้าไปใน view ซึ่งการโยนค่าเข้าไปนั้นต้องเป็น object เท่านั้นในชั้นที่1 ส่วนชั้นที่2 จะเป็น array หรืออะไรก็ได้
            });
            await next();
        } catch (err) {
            console.error(err);
            
            ctx.status = 400
            ctx.body = `Uh-oh: ${err.message}`
        }
    });
}