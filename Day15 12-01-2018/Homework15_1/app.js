const Koa = require('koa');
const app = new Koa();
const render = require('koa-ejs');
const path = require('path');

require('./controller/routes.js')(app);

render(app, {
    root: path.join(__dirname, 'views'),
    layout: 'homework10_2',
    viewExt: 'ejs',
    cache: false,
    debug: true
});
    
app.listen(3600);