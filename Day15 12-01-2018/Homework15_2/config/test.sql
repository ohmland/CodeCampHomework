create database shopStore;

CREATE TABLE supplier (
supplier_id int auto_increment primary key,
name varchar(255),
address varchar(255),
phone_number varchar(255)
);

CREATE TABLE product (
product_id int auto_increment primary key,
supplier_id int,
name varchar(255),
desc_product varchar(255),
price int,
quantity int,
foreign key (supplier_id) references supplier (supplier_id)
);

CREATE TABLE department (
department_id int auto_increment primary key,
name varchar(255) not null,
budget int
);

CREATE TABLE employee (
employee_id int auto_increment primary key,
department_id int,
name varchar(255) not null,
address varchar(255),
salary int,
foreign key (department_id) references department (department_id)
);

CREATE TABLE customer (
customer_id int auto_increment primary key,
name varchar(255),
address varchar(255)
);

CREATE TABLE orders (
orders_id int auto_increment primary key,
employee_id int,
customer_id int,
sale_date timestamp default now(),
foreign key (employee_id) references employee (employee_id),
foreign key (customer_id) references customer (customer_id)
);

CREATE TABLE orders_item (
orders_item_id int auto_increment primary key,
orders_id int,
amount int,
discount int,
foreign key (orders_id) references orders (orders_id)
);