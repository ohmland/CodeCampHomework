const {db} = require('../lib/db.js');

class User {
    constructor(rows) {
        if(rows != null){
            this.id = rows.id;
            this.firstname = rows.firstname;
            this.lastname = rows.lastname;
            this.username = rows.username;
        }
        else{
            this.id = null;
            this.firstname = null;
            this.lastname = null;
            this.username = null;
        }
        
    }

    async save() {
        if (!this.id) { //ถ้าไม่มี id เข้ามา ก็คือ สั่งให้ทำการ เพิ่มข้อมูล
                const result = await db.connection.execute(`insert into user (firstname, lastname, username) values (?, ?, ?)`, [this.firstname, this.lastname, this.username]);
                this.id = result.insertId;
                return;
        }
        else {
            return await db.connection.execute(`update user set firstname = ?, lastname = ?, username = ? where id = ?`, [this.firstname, this.lastname, this.username, this.id]);
        }
    }

    async remove() {
            return await db.connection.execute(`delete from user where id = ?`, [this.id]);
    }

    async find(id) {
        try{
            const [rows] = await db.connection.execute(`select * from user where id = ?`, [id]);
            // this.id = rows[0].id;
            // this.firstname = rows[0].firstname;
            // this.lastname = rows[0].lastname;
            //return rows[0];
            return new User(rows[0]);
        } catch (err) {
            console.error(err);
        }
    }
    async findAll() {
            const [rows] = await db.connection.execute(`select * from user`);
            //return rows.map((row) => new User(db, row));
            return rows;
    }
    async findByUsername(username) {
        const [rows] = await db.connection.execute(`select * from user where username = ?` ,[username]);
        // this.id = rows[0].id;
        // this.firstname = rows[0].firstname;
        // this.lastname = rows[0].lastname;
        // return rows[0];
        return new User(rows[0]);
    }
}

module.exports.user = new User();