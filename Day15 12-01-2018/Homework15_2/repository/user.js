function createEntity(row) {
    return {
        id: row.id,
        firstname: row.firstname,
        lastname: row.lastname,
        username: row.username
    }
}

module.exports = function (db) {
    return {
        async find(id) {
            const [rows] = await db.connection.execute(`select * from user where id = ?`, [id])
            return createEntity(rows[0])
        }
        , async findAll() {
            const [rows] = await db.connection.execute(`select * from user`)
            return rows.map(createEntity)
        }
        , async store(user) {
            if (!user.id) {
                const result = await db.connection.execute(`insert into user (firstname, lastname, username) values (?, ?, ?)`, [user.firstname, user.lastname, user.username])
                user.id = result.insertId
                return
            }
            else {
                return db.connection.execute(`update user set firstname = ?, lastname = ?, username = ? where id = ?`, [user.firstname, user.lastname, user.username, user.id])
            }
        }
        , remove(id) {
            return db.connection.execute(`delete from user where id = ?`, [id])
        }
        ,async findByUsername(username) {
            const [rows] = await db.connection.execute(`select * from user where username = ?` ,[username]);
            return rows.map(createEntity)
        }
    }
};