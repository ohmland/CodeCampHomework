import React, { Component } from 'react';
import '../App.css';
import { Layout, Form, Input, Icon, Row, Col, Button, Card, List, Spin} from 'antd';

export class Parentlink extends Component {

  constructor(props) {
    super(props);
    
    this.state = {
      inputText : '',
      listItem: []
    }

    //this.handleChangeText = this.handleChangeText.bind(this);

  }

  componentDidMount () {
    // เราควรจะ fetch เพื่อเอาค่ามาจาก MockAPI 
    this.fetchGet();
  }

  async fetchGet () {
    const result = await fetch('http://localhost:4002/list/todo')
    let data = await result.json();

    this.setState({ listItem:data , isLoading : false})
  }

  deleteListAtIndex = (index) => {
    // ไม่ควรทำเพราะเป็นการ Render ใหม่ทั้ง State ถ้ามีเยอะก็ฉิบหายยย สิครับ
    // this.state.listItem.splice(index, 1);
    // this.setState({});

    const result = this.state.listItem;
    result.splice(index, 1);
    this.setState({listItem: result});
  }

  submitList = () => {
    this.setState({
      listItem: this.state.listItem.concat([this.state.inputText]),
      inputText: ''
    })
    
    let url = 'http://localhost:4002/add/todo';
        let data = {content: this.state.inputText};
        
        fetch(url, {
        method: 'post', // or 'PUT'
        body: JSON.stringify(data),
        headers: new Headers({
        'Content-Type': 'application/json'
        })
        }).then(res => res.json())
        .catch(error => console.error('Error:', error))
        .then(response => {
          this.fetchGet();
        })
  }

  handleKeyPress = (event) => {
    if (event.key === 'Enter') {
      this.submitList();
    }
  }

  handleChangeText = (event) => {
    this.setState({inputText: event.target.value});
  }

  render() {

    // const data = [
    //     'text 1',
    //     'text 2',
    //     'text 3',
    // ];

    const { Header, Footer, Sider, Content } = Layout;
    const Search = Input.Search;
    const FormItem = Form.Item;

    return (
        <Card style={{ width: 500 , backgroundColor : this.props.myColor }}>
            <h1>To-do-list</h1>

            <div style={{ marginBottom:'10px'}}>
              <Input
                addonAfter={<Button type="primary" onClick={this.submitList}>Add</Button>}
                onChange={this.handleChangeText}
                value={this.state.inputText}
                onKeyPress={this.handleKeyPress}/>
            </div>

            <List
              bordered
              dataSource={this.state.listItem}
              renderItem={(item,index) => (
                <List.Item actions={[<a onClick={() => this.deleteListAtIndex(index)}><Icon type="close-circle" style={{ fontSize: 16, color: 'rgb(255, 145, 0)' }} /></a>]}>
                    {item.content}
                </List.Item>
            )}
            />
            {/*
              this.state.listItem.map((value, index) => {
                //console.log(index);
                return (
                  <h3 key={index + value}>{value}</h3>
                );
              })
            */}
        </Card>
      );
    }
}
