import React, { Component } from 'react';
import '../App.css';
import { Layout, Form, Input, Icon, Row, Col, Button, Card, List, Spin} from 'antd';
import firebase from 'firebase';

export class Firebaselink extends Component {

  constructor(props) {
    super(props);
    
    // Initialize Firebase
    var config = {
      apiKey: "AIzaSyC4lUvi9v7huxIUP9I_4JECC_P9aGicCEQ",
      authDomain: "homework-bcbff.firebaseapp.com",
      databaseURL: "https://homework-bcbff.firebaseio.com",
      projectId: "homework-bcbff",
      storageBucket: "homework-bcbff.appspot.com",
      messagingSenderId: "156969651603"
    };
    this.conn = firebase.initializeApp(config);

    this.state = {
      inputText : '',
      listItem: []
    }

    //this.handleChangeText = this.handleChangeText.bind(this);

  }

  componentDidMount () {
    // เราควรจะ fetch เพื่อเอาค่ามาจาก MockAPI 
    this.fetchGet();
  }

  async fetchGet () {
    this.conn.database().ref('/').on('value', snapshot => {
      if(snapshot.val()){
        let data = Object.entries(snapshot.val()).map((data, index)=>{
          return Object.values(data[1])
        })
        console.log(data[0]);
        this.setState({
          listItem: data[0],
          isLoading: false
        })
      }else{
        this.setState({
          isLoading: false
        })
      }
      
    })

    // const result = await fetch('http://5a7134f0ce7c440012e89ecf.mockapi.io/todo')
    // let data = await result.json();

    // this.setState({ listItem:data , isLoading : false})
  }

  deleteListAtIndex = (index) => {
    // ไม่ควรทำเพราะเป็นการ Render ใหม่ทั้ง State ถ้ามีเยอะก็ฉิบหายยย สิครับ
    // this.state.listItem.splice(index, 1);
    // this.setState({});

    // const result = this.state.listItem;
    // result.splice(index, 1);
    // this.setState({listItem: result});

    this.conn.database().ref('messages/'+index).remove()
    .then(function(){
      console.log("Remove succeded.");
    })
    .catch(function(error){
      console.log("Remove failed:" + error.message);
    })
  }

  submitList = () => {
    let key = this.conn.database().ref('/').push().key
    this.conn.database().ref('messages/'+key).set({
      id:key,
      msg: this.state.inputText
      
    })

    // this.setState({
    //   listItem: this.state.listItem.concat([this.state.inputText]),
    //   inputText: ''
    // })
    
    // let url = 'http://5a7134f0ce7c440012e89ecf.mockapi.io/todo';
    //     let data = {content: this.state.inputText};
        
    //     fetch(url, {
    //     method: 'post', // or 'PUT'
    //     body: JSON.stringify(data),
    //     headers: new Headers({
    //     'Content-Type': 'application/json'
    //     })
    //     }).then(res => res.json())
    //     .catch(error => console.error('Error:', error))
    //     .then(response => {
    //       this.fetchGet();
    //     })

    this.setState({
      inputText: ''
    })
  }

  handleKeyPress = (event) => {
    if (event.key === 'Enter') {
      this.submitList();
    }
  }

  handleChangeText = (event) => {
    this.setState({inputText: event.target.value});
  }

  render() {

    // const data = [
    //     'text 1',
    //     'text 2',
    //     'text 3',
    // ];

    const { Header, Footer, Sider, Content } = Layout;
    const Search = Input.Search;
    const FormItem = Form.Item;

    return (
        <Card style={{ width: 500 , backgroundColor : this.props.myColor }}>
            <h1>To-do-list</h1>

            <div style={{ marginBottom:'10px'}}>
              <Input
                addonAfter={<Button type="primary" onClick={this.submitList}>Add</Button>}
                onChange={this.handleChangeText}
                value={this.state.inputText}
                onKeyPress={this.handleKeyPress}/>
            </div>

            <List
              bordered
              dataSource={this.state.listItem}
              renderItem={(item,index) => (
                <List.Item actions={[<a onClick={() => this.deleteListAtIndex(item.id)}><Icon type="close-circle" style={{ fontSize: 16, color: 'rgb(255, 145, 0)' }} /></a>]}>
                    {item.msg}
                </List.Item>
            )}
            />
            {/*
              this.state.listItem.map((value, index) => {
                //console.log(index);
                return (
                  <h3 key={index + value}>{value}</h3>
                );
              })
            */}
        </Card>
      );
    }
}
