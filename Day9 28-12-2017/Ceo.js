const {Employee} = require('./employee.js');

let fs = require('fs');

class CEO extends Employee {
    constructor(firstname, lastname, salary, id, dressCode) {
        super(firstname, lastname, salary);
        this.id = id;
        this.dressCode = 'tshirt';
        this.employeesRaw = [];
        let self = this;
    }
    getSalary() { // simulate public method
        return super.getSalary() * 2;
    };
    work(employee) { // simulate public method
        this._fire(employee);
        this._hire(employee);
        this._seminar();
        this._golf();
    }
    increaseSalary(employee, newSalary) {
        if (employee.setSalary(newSalary)) {
            console.log(employee.firstname + "'s salary is increase to " + employee.getSalary());
        } else {
            console.log(employee.firstname + "'s salary is less than before!!");
        }
        //Somsri's salary is less than before!!
        //Somsri's salary is less than before!!
    }
    _golf() { // simulate private method
        this.dressCode = 'golf_dress';
        console.log("He goes to golf club to find a new connection." + " Dress with :" + this.dressCode);

    };
    _fire(employee) {
        this.dressCode = 'tshirt';
        console.log(employee.firstname + " has been fired!" + " Dress with :" + this.dressCode);
    }
    _hire(employee) {
        this.dressCode = 'tshirt';
        console.log(employee.firstname + " has been hired back!" + " Dress with :" + this.dressCode);
    }
    _seminar() {
        this.dressCode = 'suit';
        console.log("He is going to seminar" + " Dress with :" + this.dressCode);
    }

    async readFile(callback) {
        try {
            let data = await this.readData();
            this.employeesRaw = JSON.parse(data);
            //return this.employeesRaw;
            callback(null,this.employeesRaw);
        } catch (error) {
            console.error(error);
        }
    }

    readData() {
        return new Promise(function (resolve, reject) {
            fs.readFile(__dirname+'/employee9.json', 'utf8', function (err, dataHW1) {
                if (err)
                    reject(err);
                else
                    resolve(dataHW1);
            });
        });
    }
    
    talk(message){
        console.log(message);
    }

    reportRobot(self, robotMessage) {
        self.talk(robotMessage);
    }

}

exports.CEO = CEO;