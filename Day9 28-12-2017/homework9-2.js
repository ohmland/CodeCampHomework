const {Employee} = require('./employee.js');
const {Programmer} = require('./Programmer.js');
const {CEO} = require('./Ceo.js');
const {OfficeCleaner} = require('./OfficeCleaner.js');

let nakorn = new CEO("Nakorn","Bun",30000,1001,'tshirt');

let nene = new OfficeCleaner("Nene","nene",10000,1011,'Maid');
nakorn.readFile(function fcName(err, data) {
    if (err) {
        console.error(err);
    } else {
        let employees = [];
        for (let i in data) {
            if(data[i].role === 'CEO'){
                employees[i] = new CEO(data[i].firstname, data[i].lastname, data[i].salary, data[i].id, data[i].dressCode);
            } else if(data[i].role === 'Programmer'){
                employees[i] = new Programmer(data[i].firstname, data[i].lastname, data[i].salary, data[i].id, data[i].type);
            } else if(data[i].role === 'OfficeCleaner'){
                employees[i] = new OfficeCleaner(data[i].firstname, data[i].lastname, data[i].salary, data[i].id, data[i].dressCode);
            }
        }

        for (let item in employees) {
            employees[item].work(nene);
        }
    }
});