const {Employee} = require('./employee.js');
let fs = require('fs');

class OfficeCleaner extends Employee {
    constructor(firstname, lastname, salary, id, dressCode) {
        super(firstname, lastname, salary);
        this.id = id;
        this.dressCode = dressCode;
    }
    work(employee) { // simulate public method
        this.Clean();
        this.KillCoachroach();
        this.DecorateRoom();
        this.WelcomeGuest();
    }
    Clean() { // simulate private method
        console.log("Clean");
    }
    KillCoachroach() {
        console.log("Kill Coachroach");
    }
    DecorateRoom() {
        console.log("Decorate Room");
    }
    WelcomeGuest() {
        console.log("Welcome Guest");
    }

}

exports.OfficeCleaner = OfficeCleaner;