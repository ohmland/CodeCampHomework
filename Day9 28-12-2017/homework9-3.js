const _ = require('lodash');

class MyUtility {
    assign(object, sources){
        _.assign(object, sources)
    }
    times(n, iteratee=_.identity){
        _.times(n,iteratee)
    }
    keyBy(collection, iteratee=_.identity){
        _.keyBy(collection, iteratee)
    }
    cloneDeep(value){
        _.cloneDeep(value)
    }
    filter(collection, predicate=_.identity){
        _.filter(collection, predicate)
    }
    sortBy(collection, iteratees=[_.identity]){
        _.sortBy(collection, iteratees)
    }
}