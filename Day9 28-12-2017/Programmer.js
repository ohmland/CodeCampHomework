const {Employee} = require('./employee.js');

class Programmer extends Employee {
    constructor(firstname, lastname, salary, id, type) {
        super(firstname, lastname, salary);
        this.id = id;
        this.type = type;
    }
    work(employee) { // simulate public method
        this.CreateWebsite();
        this.FixPC();
        this.InstallWindows();
    }

    CreateWebsite() {
        console.log('CreateWebsite');
    }
    FixPC() {
        console.log('FixPC');
    }
    InstallWindows() {
        console.log('InstallWindows');
    }
}


exports.Programmer = Programmer;