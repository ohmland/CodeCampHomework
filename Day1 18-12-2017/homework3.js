let peopleSalary = [
    {"id":"1001","firstname":"Luke","lastname":"Skywalker","company":"Walt Disney","salary":"40000"},
    {"id":"1002","firstname":"Tony","lastname":"Stark","company":"Marvel","salary":"1000000"},
    {"id":"1003","firstname":"Somchai","lastname":"Jaidee","company":"Love2work","salary":"20000"},
    {"id":"1004","firstname":"Monkey D","lastname":"Luffee","company":"One Piece","salary":"9000000"}
]

let temp=[];
for (let i = 0; i < peopleSalary.length; i++) { 
    let obj = {};
    for(let item in peopleSalary[i]) {
        if(item!=="company" && item!=="salary")
        {
            obj[item]=peopleSalary[i][item];
        }
        else if(item==="salary")
        {
            let salaryArr = [];
            salaryArr.push(peopleSalary[i][item]);
            salaryArr.push(peopleSalary[i][item]*1.1);
            let newSalary = peopleSalary[i][item]*1.1;
            salaryArr.push(newSalary+(newSalary*10)/100);
            //salaryArr.push("2");
            //salaryArr.push(peopleSalary[i][item]);
            obj[item]=salaryArr;
        }
    }
    temp.push(obj);
}
console.log(temp);