import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import Hw1 from './homework29_1';
import Hw2 from './homework29_2';
import Hw3 from './homework29_3';
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(<Hw2/>, document.getElementById('root'));
registerServiceWorker();