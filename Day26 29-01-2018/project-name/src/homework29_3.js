import React, { Component } from 'react';
import { Card, Form, Icon, Input, Button, Checkbox, Layout, Menu, Breadcrumb, Carousel, Row, Col, Rate } from 'antd'
import './App.css';
import './homework29_3.css';
import logo from './mayzz.png';
import { Anchor } from 'antd';
const { Link } = Anchor;
const { SubMenu } = Menu;
const { Header, Content, Footer, Sider } = Layout;


class App extends Component {
    render() {
        const FormItem = Form.Item;
        return (
            <div>
                <Row>
                    <Col span={12}>
                        <div className = "contentTw">
                            <img src={logo} alt="logo" className="bgBlueSky" />
                            <p className="fontContentLeft centered1"><Icon type="search" />&nbsp;&nbsp;ติดตามสิ่งที่คุณสนใจ</p>
                            <p className="fontContentLeft centered2"><Icon type="team" />&nbsp;&nbsp;ฟังสิ่งที่ผู้คนกำลังพูดถึง</p>
                            <p className="fontContentLeft centered3"><Icon type="message" />&nbsp;&nbsp;เข้าร่วมบทสนทนา</p>
                        </div>
                    </Col>
                    <Col span={12}>
                        <div className = "contentTw">
                            <Form>
                                <FormItem className="martop">
                                    <Col span={8}>
                                        <div>
                                            <Input placeholder="โทรศัพท์ อีเมล หรือชื่อผู้ใช้" />
                                        </div>
                                    </Col>
                                    <Col span={8}>
                                        <Input type="password" placeholder="รหัสผ่าน" />
                                        <div>
                                            <p className="forgotpass">ลืมรหัสผ่าน?</p>
                                        </div>
                                    </Col>
                                    <Col span={8}>
                                        <Button className="btntop">เข้าสู่ระบบ</Button>
                                    </Col>
                                </FormItem>
                            </Form>
                        </div>
                        <div>
                        <Row>
                            <Col span={24} style={{ textAlign: 'center', paddingTop: '2em' }}>
                            <div style={{ width: 400, marginLeft: 'auto', marginRight: 'auto', marginTop: 200, textAlign: 'left' }}>
                                <Icon type="twitter" style={{ color: '#1DA1F2', fontSize: '300%' }} />
                                <h1>See what's happening in <br/> the world right now <br/></h1>
                                <h3>Join Twitter today. <br/><br/></h3>
                                <Form onSubmit={this.handleSubmit} className="login-form">
                                <FormItem>
                                    <Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Username" />
                                </FormItem>
                                <FormItem>
                                    <Input prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder="Password" />
                                </FormItem>
                                <FormItem>
                                    <Button type="primary" htmlType="submit" className="login-form-button">
                                    Get Started
                                    </Button>
                                    &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
                                    Have an account? <a href="">Login</a>
                                </FormItem>
                                </Form>
                            </div>
                            </Col>
                        </Row>
                        </div>
                    </Col>
                </Row>
                <Row>
                    <Col span={24} className='footer' style={{ textAlign: 'center' }}>
                        <div className="footerBar">
                        About&emsp;
                        Help Center&emsp;
                        Blog&emsp;
                        Status&emsp;
                        Jobs&emsp;
                        Terms&emsp;
                        Privacy Policy&emsp;
                        Cookies&emsp;
                        Ads info&emsp;
                        Brand&emsp;
                        Apps&emsp;
                        Advertise&emsp;
                        Marketing&emsp;
                        Businesses&emsp;
                        Developers&emsp;
                        Directory&emsp;
                        Settings&emsp;
                        © 2018 Twitter
                        </div>
                    </Col>
                </Row>
            </div>
        );
    }
}

export default App;