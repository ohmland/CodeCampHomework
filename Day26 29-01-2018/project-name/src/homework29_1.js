import React, { Component } from 'react';
//import logo from './logo.svg';
import './App.css';
import './homework29_1.css';
//import Form from 'antd/lib/form/Form';
import { Card } from 'antd';
import { Form, Icon, Input, Button, Checkbox } from 'antd';

class App extends Component {
  render() {
    const FormItem = Form.Item;

    return (
      <div className="App">
            <h1>Welcome to my application.</h1>
            <Card className="center" title="Sign In">
                <Form className="login-form">
                    <FormItem>
                        <div className="marbtn">
                            <h3>User Name:</h3>
                            <Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Username" />
                            <h3>Password:</h3>
                            <Input prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder="Password" />
                        </div>
                        <div>
                            <Button type="primary" htmlType="submit" className="login-form-button">
                            SIGN IN
                            </Button>
                        </div>
                            <a className="login-form-forgot" href="">Forgot password?</a><hr/>
                        <div>
                            <Button type="primary" htmlType="submit" className="login-form-button">
                            SIGN UP
                            </Button>
                        </div>
                    </FormItem>
                </Form>
            </Card>
      </div>
    );
  }
}

export default App;
