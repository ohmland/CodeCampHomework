const Koa = require('koa');
const bodyParser = require('koa-bodyparser');
const bcrypt = require('bcryptjs');

const app = new Koa();

app.use(bodyParser());
let ix = 0;
app.use(async(ctx) => {
    //const {passwords} = ctx.request.body  //{"passwords" : "1234"} //1234
    const {passwords} = {"passwords" : "1234"} //1234
    
    if (!passwords){
        ctx.throw(400);
    }

    console.log(ctx.path);
    
    ix++;
    const hash = await bcrypt.hash(passwords, 10);
    ctx.body = {hash, passwords};
    console.log(ix);
    console.log(hash);
    
});

app.listen(3000);