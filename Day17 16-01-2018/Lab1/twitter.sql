create table user (
    id int auto_increment,
    username varchar(30) not null,
    email varchar(255) not null,
    password varchar(255) not null,
    name varchar(40) not null,
    photo varchar(255) not null default '',
    cover varchar(255) not null default '',
    theme varchar(10) not null default '',
    location varchar(100) not null default '',
    bio varchar(255) not null default '',
    birth_date_d int,
    birth_date_m int,
    birth_date_y int,
    show_dm boonlean not null default false,
    show_y boonlean not null default false,
    create_at timestamp not null default now(),
    primary key (id),
    unique (username),
    unique (email)
);

create table follows (
    follower_id int,
    following_id int,
    create_at timestamp not null default now(),
    primary key (follower_id, following_id),
    foreign key (follower_id) references user (id),
    foreign key (following_id) references user (id)
);

create table tweets (
    id int auto_increment,
    user_id int not null,
    content varchar(140) not null,
    type int not null,
    create_at timestamp not null default now(),
    primary key (id),
    foreign key (user_id) references user (id)
);

create table tweet_photos (
    id int auto_increment,
    tweet_id int,
    url varchar(255) not null,
    primary key (id),
    foreign key (tweet_id) references tweets (id)
);

create table hashtags (
    name varchar(30),
    create_at timestamp not null default now(),
    primary key (id)
);

create table tweet_hashtags (
    tweet_id int,
    hashtags varchar(30),
    primary key (tweet_id, hashtag),
    foreign key (tweet_id) references tweets (id),
    foreign key (hashtag) references hashtags (name)
);

create table tweet_polls (
    tweet_id int,
    end_time timestamp not null,
    primary key (tweet_id),
    foreign key (tweet_id) references tweets (id)
);

create table tweet_choices (
    id int auto_increment,
    tweet_id int not null,
    content varchar(25),
    primary key (id),
    foreign key (tweet_id) references tweet_polls (tweet_id)
);

create table votes (
    user_id int,
    tweet_id int,
    choice_id int not null,
    create_at timestamp not null default now(),
    primary key (user_id, tweet_id),
    foreign key (user_id) references user (id),
    foreign key (tweet_id) references tweets (id)
);

create table retweets (
    user_id int,
    tweet_id int,
    content varchar(140) not null default '',
    create_at timestamp not null default now(),
    primary key (user_id, tweet_id),
    foreign key (user_id) references user (id),
    foreign key (tweet_id) references tweets (id)
);

create table tweet_replies (
    id int auto_increment,
    tweet_id int not null,
    user_id int not null,
    content varchar(140) not null default '',
    create_at timestamp not null default now(),
    primary key (id),
    foreign key (tweet_id) references tweets (id),
    foreign key (user_id) references user (id)
);

create table user_chats (
    id int auto_increment,
    sender_id int not null,
    receiver_id int not null,
    content varchar(255) not null,
    type int not null,
    create_at timestamp not null default now(),
    primary key (id),
    foreign key (sender_id) references user (id),
    foreign key (receiver_id) references user (id)
);

create table notifications (
    id int auto_increment,
    user_id int not null,
    title varchar(60) not null,
    content varchar(80) not null,
    photo varchar(255) not null,
    is_read boonlean not null default false,
    create_at timestamp not null default now(),
    primary key (id),
    foreign key (user_id) references user (id)
);