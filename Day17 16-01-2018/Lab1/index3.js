const Koa = require('koa')
const session = require('koa-session')

const app = new Koa()

const sessionStore = {}

app.keys = ['supersecret']

const sessionConfig = {
    key: 'sess',
    maxAge: 3600,
    httpOnly: true,
    store: {
        get(key, maxAge, {rolling}) {
            return sessionStore[key]
        },
        set(key, sess, maxAge, {rolling}) {
            sessionStore[key] = sess
        },
        destroy(key) {
            delete sessionStore[key]
        }
    }
}

app.use(session(sessionConfig, app))
   .use(handler)
   .listen(3000)

function handler(ctx) {
    if(ctx.path != '/favicon.ico') {
        let n = ctx.session.views || 0
        ctx.session.views = ++n
        ctx.body = `${n} views`
        console.log(sessionStore);
        
    }
}