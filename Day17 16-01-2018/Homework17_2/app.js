const Koa = require('koa');
const session = require('koa-session');
const app = new Koa();
const path = require('path');
const config = require('./config/database.json');
const db = require('./lib/db.js');
const bodyParser = require('koa-bodyparser');
const bcrypt = require('bcryptjs');
const Router = require('koa-router')

const router = new Router()
    .get('/', index)
    .post('/auth/signup', signup)

async function index(ctx) {
    ctx.body = "index;"
}

async function signup(ctx) {
    console.log(ctx.request.body);
    
    const {username, password, email, name} = ctx.request.body

    //console.log('Request Body:', ctx.request)

    if (!password) {
        ctx.throw(400);
    }

    const hashPass = await bcrypt.hash(password, 10);
    try{
        await save([username, hashPass, email, name]);
        ctx.body = {}
    } catch(err){
        ctx.body = {"error": "some error"}
    }
    
}

async function name() {
    const db1 = await db(config);
    const [data] = await db1.execute('select * from users');
    //console.log(data);
}

async function save([username, hashPass, email, name]) {
    
    const db1 = await db(config);
    await db1.execute("INSERT INTO `users`(`username`, `password`, `email`, `name`) VALUES (?,?,?,?)", [username, hashPass, email, name]);
    
    // const [data] = await db1.execute('select * from users');
    // console.log(data);
    //let a = result.insertId;
    // console.log(result[0].insertId);
    // //console.log(a);

    // if(result[0].insertId){
    //     return "success";
    // }
    // else{
    //     return "fail";
    // }

}

name();

app.use(bodyParser());
app.use(router.routes()).listen(4500);