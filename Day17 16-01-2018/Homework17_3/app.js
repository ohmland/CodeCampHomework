const Koa = require('koa');
const app = new Koa();
const session = require('koa-session');
const path = require('path');
const config = require('./config/database.json');
const {db} = require("./lib/db.js");
const mysess = require("./models/mysess.js")(db);
const bodyParser = require('koa-bodyparser');
const bcrypt = require('bcryptjs');
const Router = require('koa-router')
//const sessionStore = {};
//const mysess = require("./models/mysess.js")(db);

const sessionConfig = {
    key: 'sess',
    maxAge: 3600 * 1000,
    httpOnly: true,
    store: mysess
};

const router = new Router()
    .get('/', index)
    .post('/auth/signin', login)
    .post('/auth/signout', logout)
    .get('/auth/check', checkSession)

    if(ctx.session){

    }else{
        
    }

async function index(ctx) {
    ctx.body = "index;"
}

async function login(ctx) {
    const {email, password} = ctx.request.body
    const [result] = await db.connection.execute("select * from `users` where email = ? and password = ?", [email, password]);
    let data = [result][0];
    console.log(data);
    
        
    if(data.length > 0){
        let data = [result][0];
        ctx.session.email = data[0].email;
        ctx.status = 200;
        ctx.body = data;
        //ctx.body = {};
    }else{
        ctx.status = 401;
        ctx.body = {"error": "wrong password"}
    }
}

async function logout(ctx) {
    ctx.status = 200;
    ctx.session = null;
    ctx.body = {};
}

async function checkSession(ctx) {
    if(ctx.session.email){
        ctx.status = 200;
        ctx.body = {};
    }else{
        ctx.status = 401;
        ctx.body = {
            "error": "unauthorized"
        };
    }
}

app.keys = ['supersecret'];
app.use(bodyParser())
    .use(session(sessionConfig, app))
    .use(router.routes()).listen(6000);