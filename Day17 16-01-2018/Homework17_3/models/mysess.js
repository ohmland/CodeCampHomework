module.exports = function (db) {
    return {
        async get(key, maxAge, {rolling}) {
            let result = await db.connection.execute("SELECT `sess_sess` FROM `user_session` WHERE `sess_key` = ?", [key]);
            if (Array.isArray(result)) {
                if (result.length > 0) {
                    result = result[0]
                    console.log(result[0].sess_sess);
                    let str = JSON.parse(result[0].sess_sess);
                    
                    
                    return str;
                }
            }
            return;
            //return sessionStore[key]
        },
        async set(key, sess, maxAge, {rolling}) {
            //let result = await db.query("SELECT `sess_sess` FROM `sessionstore` WHERE `sess_key` = ?", [key]);
            let str = JSON.stringify(sess);
            await db.connection.execute(`INSERT INTO user_session (sess_key, sess_sess) VALUES (?,?) on duplicate key update sess_sess = ?`, [key, str, str]);
            //sessionStore[key] = sess

        },
        async destroy(key) {
            await db.connection.execute("DELETE FROM `user_session` WHERE sess_key = ?", [key]);
            return;
            //delete sessionStore[key]
        }
    }
}