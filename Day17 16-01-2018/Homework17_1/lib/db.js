class Database {
    constructor( ) {    
                this.host = '127.0.0.1';
                this.user = 'root';
                this.database = 'sessiondb';
                this.connectDatabase(this.host,this.user,this.database);
    }

    async connectDatabase(host,user,database) {
        // get the client
        const mysql = require('mysql2/promise');
        
        // create the connection
        this.connection = await mysql.createConnection({
            host: this.host,
            user: this.user,
            database: this.database
        });
    }

    // async getData() {
    //     const [rows, fields] = await db.connection.execute('SELECT * FROM user');
    //     return rows;
    // }
}

module.exports.db = new Database();