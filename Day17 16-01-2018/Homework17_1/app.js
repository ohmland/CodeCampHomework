const Koa = require('koa');
const session = require('koa-session');
const sessionStore = {};
const {db} = require("./lib/db.js");
const mysess = require("./models/mysess.js")(db);
//const todoSess = mysess(db);
let check = 0;
const sessionConfig = {
    key: 'sess',
    maxAge: 3600 * 1000,
    httpOnly: true,
    store: mysess
};

const app = new Koa();
app.keys = ['supersecret'];
app.use(session(sessionConfig, app))
    .use(handler)
    .listen(3500);

function handler (ctx) {
    if(ctx.path != '/favicon.ico'){
        let n = ctx.session.views || 0
        ctx.session.views = ++n
        ctx.body = `${n} views`
    }
}

//---------------------------------------------------
// store: {
//     async get(key, maxAge, {rolling}) {
//         let result = await db.connection.execute("SELECT `sess_sess` FROM `sessionstore` WHERE `sess_key` = ?", [key]);
//         console.log(result);
//         if (Array.isArray(result)) {
//             if (result.length > 0) {
//                 console.log(`result = ${result[0].sess_sess}`);
//                 let sss = new Buffer(result[0].sess_sess, 'base64').toString('ascii');
//                 sss = JSON.parse(sss);
//                 console.log(`sss = ${sss}`);
//                 return sss;
//             }
//         }
//         return;
//         //return sessionStore[key]
//     },
//     async set(key, sess, maxAge, {rolling}) {
//         //let result = await db.query("SELECT `sess_sess` FROM `sessionstore` WHERE `sess_key` = ?", [key]);
//         let sess_base64 = await new Buffer(JSON.stringify(sess)).toString('base64');
//         await db.connection.execute("INSERT INTO `sessionstore`(`sess_key`, `sess_sess`) VALUES (?,?)", [key, sess_base64]);
//         //sessionStore[key] = sess

//     },
//     async destroy(key) {
//         await db.connection.execute("DELETE FROM `sessionstore` WHERE sess_key = ?", key);
//         console.log("Event destroyed.");
//         return;
//         //delete sessionStore[key]
//     }
// }