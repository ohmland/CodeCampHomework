import React, { Component } from 'react';
//import logo from './logo.svg';
import './App.css';
import './Homework2.css';

import {Button} from 'antd';

class Numpad extends Component {
  render() {
    return (
        <div>
            <div className="bordercalculate" >
                <div>
                    <Button onClick={() => this.props.onTest('7')}>7</Button>
                    <Button onClick={() => this.props.onTest('8')}>8</Button>
                    <Button onClick={() => this.props.onTest('9')}>9</Button>
                    <Button onClick={() => this.props.onTest('÷')}>÷</Button>
                </div>
                <div>
                    <Button onClick={() => this.props.onTest('4')}>4</Button>
                    <Button onClick={() => this.props.onTest('5')}>5</Button>
                    <Button onClick={() => this.props.onTest('6')}>6</Button>
                    <Button onClick={() => this.props.onTest('x')}>x</Button>
                </div>
                <div>
                    <Button onClick={() => this.props.onTest('1')}>1</Button>
                    <Button onClick={() => this.props.onTest('2')}>2</Button>
                    <Button onClick={() => this.props.onTest('3')}>3</Button>
                    <Button onClick={() => this.props.onTest('+')}>+</Button>
                </div>
                <div>
                    <Button onClick={() => this.props.onTest('C')}>C</Button>
                    <Button onClick={() => this.props.onTest('0')}>0</Button>
                    <Button onClick={() => this.props.onTest('.')}>.</Button>
                    <Button onClick={() => this.props.onTest('=')}>=</Button>
                </div>
            </div>
        </div>
      );
  }
}

export default Numpad;
