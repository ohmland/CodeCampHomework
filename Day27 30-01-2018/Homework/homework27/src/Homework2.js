import React, { Component } from 'react';
//import logo from './logo.svg';
import './App.css';
import './Homework2.css';
import {Card} from 'antd';
//import { Layout, Form, Input, Icon, Row, Col, Button, Card, List, Avatar } from 'antd';
import Numpad from './Numpad';
import Display from './Display';

class Homework2 extends Component {
  state = {value1: 0,
            operator: "",
            value2: ""
        }

  onTest = (value) => {
      console.log(value);
      
      if(value == "+"){
        this.setState({operator:'+'})
      }else if(value == "-"){
        this.setState({operator:'-'})
      }else if(value == "x"){
        this.setState({operator:'x'})
      }else if(value == "÷"){
        this.setState({operator:'÷'})
      }else if(value == "C"){
        this.setState({value1 : 0,
            value2:"",
            operator:""
        })
      }else if(value == "="){ //เหลือ parse ค่า จาก string ให้้เป็น เลขก่อน ทั้งสองค่า
          console.log(this.state.operator);
          
            if(this.state.operator == '+'){
                let tempResult = parseInt(this.state.value1) + parseInt(this.state.value2)
                this.setState({value1 : tempResult,
                    value2:"",
                    operator:""
                })
            }else if(this.state.operator == '-'){
                let tempResult = parseInt(this.state.value1) - parseInt(this.state.value2)
                this.setState({value1 : tempResult,
                    value2:"",
                    operator:""
                })
            }else if(this.state.operator == 'x'){
                let tempResult = parseInt(this.state.value1) * parseInt(this.state.value2)
                this.setState({value1 : tempResult,
                    value2:"",
                    operator:""
                })
            }else if(this.state.operator == '÷'){
                if(parseInt(this.state.value2) != 0){
                        let tempResult = parseInt(this.state.value1) / parseInt(this.state.value2)
                        this.setState({value1 : tempResult,
                        value2:"",
                        operator:""
                    })
                }else{
                    this.setState({value1 : "NaN",
                        value2:"",
                        operator:""
                    })
                }
                
            }
        }else{
            if(this.state.operator == ""){//กรณีที่กำลังจะกดค่าแรก value1
                if(this.state.value1 == 0){ //เช็คว่าตัวแรกเป็น 0 มั้ย
                switch (value) {
                    case '.':
                        this.setState({value1:0.})
                        //this.concat(value)
                        break;
                    case '0':
                        this.setState({value1:value})
                        break;
                    case '1':
                        this.setState({value1:value})
                        break;
                    case '2':
                        this.setState({value1:value})
                        break;
                    case '3':
                        this.setState({value1:value})
                        break;
                    case '4':
                        this.setState({value1:value})
                        break;
                    case '5':
                        this.setState({value1:value})
                        break;
                    case '6':
                        this.setState({value1:value})
                        break;
                    case '7':
                        this.setState({value1:value})
                        break;
                    case '8':
                        this.setState({value1:value})
                        break;
                    case '9':
                        this.setState({value1:value})
                    }
                }
                else{ //กรณีที่ตัวแรกไม่ได้เป็น 0 แล้ว
                    switch (value) {
                        case '.':
                            //this.setState({value})
                            this.concat(value)
                            break;
                        case '0':
                            //this.setState({value})
                            this.concat(value)
                            break;
                        case '1':
                            // this.setState({value})
                            this.concat(value)
                            break;
                        case '2':
                            // this.setState({value})
                            this.concat(value)
                            break;
                        case '3':
                            // this.setState({value})
                            this.concat(value)
                            break;
                        case '4':
                            // this.setState({value})
                            this.concat(value)
                            break;
                        case '5':
                            // this.setState({value})
                            this.concat(value)
                            break;
                        case '6':
                            // this.setState({value})
                            this.concat(value)
                            break;
                        case '7':
                            // this.setState({value})
                            this.concat(value)
                            break;
                        case '8':
                            // this.setState({value})
                            this.concat(value)
                            break;
                        case '9':
                            // this.setState({value})
                            this.concat(value)
                        }
                    }
              }else if(this.state.operator!=""){//กรณีที่กำลังจะใส่ค่าที่สอง value2
                if(this.state.value2 == 0){ //กรณีที่ยังไม่ได้กดค่าแรก
                    switch (value) {
                        case '.':
                            this.setState({value2:0.})
                            //this.concat(value2)
                            break;
                        case '0':
                            this.setState({value2:0})
                            break;
                        case '1':
                            this.setState({value2:1})
                            break;
                        case '2':
                            this.setState({value2:2})
                            break;
                        case '3':
                            this.setState({value2:3})
                            break;
                        case '4':
                            this.setState({value2:4})
                            break;
                        case '5':
                            this.setState({value2:5})
                            break;
                        case '6':
                            this.setState({value2:6})
                            break;
                        case '7':
                            this.setState({value2:7})
                            break;
                        case '8':
                            this.setState({value2:8})
                            break;
                        case '9':
                            this.setState({value2:9})
                        }
                }
                else{ //กรณีที่กดค่าที่ 2
                    switch (value) {
                        case '.':
                            //this.setState({value})
                            this.concatVal2(value)
                            break;
                        case '0':
                            //this.setState({value})
                            this.concatVal2(value)
                            break;
                        case '1':
                            //this.setState({value})
                            this.concatVal2(value)
                            break;
                        case '2':
                            //this.setState({value})
                            this.concatVal2(value)
                            break;
                        case '3':
                            //this.setState({value})
                            this.concatVal2(value)
                            break;
                        case '4':
                            //this.setState({value})
                            this.concatVal2(value)
                            break;
                        case '5':
                            //this.setState({value})
                            this.concatVal2(value)
                            break;
                        case '6':
                            //this.setState({value})
                            this.concatVal2(value)
                            break;
                        case '7':
                            //this.setState({value})
                            this.concatVal2(value)
                            break;
                        case '8':
                            //this.setState({value})
                            this.concatVal2(value)
                            break;
                        case '9':
                            //this.setState({value})
                            this.concatVal2(value)
                    }
              }
              
            }
      }

      
  }

  concat = (value) => {
    let temp = this.state.value1;
    let result = temp+value;
    
    this.setState({
        value1:result
    });
  }

  concatVal2 = (value) => {
    let temp = this.state.value2;
    let result = temp+value;
    
    this.setState({
        value2:result
    });
  }

  render = () => (
    <Card className="appcenter" style={{ width: 500 , backgroundColor : this.props.myColor }}>
        <h1>Calculate</h1>
        <div style={{ marginBottom:'10px'}}>
            <Display value1={this.state.value1} operator={this.state.operator} value2={this.state.value2} />
            <Numpad onTest={this.onTest} />
        </div>
    </Card>
    )
}

export default Homework2;