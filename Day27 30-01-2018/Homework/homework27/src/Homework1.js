import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import './Homework1.css';
import { Layout, Form, Input, Icon, Row, Col, Button, Card, List, Avatar } from 'antd';

class Homework1 extends Component {

  constructor(props) {
    super(props);

    this.state = {
      inputTextqqqq : '',
      listItem: []
    }

    

    //this.handleChangeText = this.handleChangeText.bind(this);
  }

  submitList = () => {
    this.setState({
      listItem: this.state.listItem.concat([this.state.inputText]),
      inputText: ''
    })
  }

  handleKeyPress = (event) => {
    if (event.key === 'Enter') {
      this.submitList();
    }
  }

  handleChangeText = (event) => {
    this.setState({inputText: event.target.value});
  }

  render() {
    const { Header, Footer, Sider, Content } = Layout;
    const Search = Input.Search;
    const FormItem = Form.Item;

    return (
        <Card className="appcenter" style={{ width: 500 , backgroundColor : this.props.myColor }}>
            <h1>ศาลาคนเศร้า</h1>
            <List dataSource={this.state.listItem} renderItem={(item,index) => (
              index % 2 == 0 ?
                <List.Item className="contentright">
                  <List.Item.Meta
                  avatar={<Avatar src="http://thaipublica.org/infographic/images/icon-gov03.png" />}
                  title={<b>ลุงไง</b>}
                  description={item}/>
                </List.Item>
                :
                <List.Item className="contentleft">
                  <List.Item.Meta
                  avatar={<Avatar src="./prayut.png" />}
                  title={<b>วิทย์เอง</b>}
                  description={item}/>
                </List.Item>
            )}
            />
            <div style={{ marginBottom:'10px'}}>
              <Input
                addonAfter={<Button type="primary" onClick={this.submitList}>Send</Button>}
                onChange={this.handleChangeText}
                value={this.state.inputText}
                onKeyPress={this.handleKeyPress}/>
            </div>
        </Card>
      );
  }
}

export default Homework1;